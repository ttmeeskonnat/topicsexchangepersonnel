'use strict';

personnelModule.factory("customer", ['$http', function ($http) {
  return {
    getTeachers: function (cbFn) {
      invokeGet($http, 'customers/teachers', cbFn);
    },

    getStudents: function (cbFn) {
      invokeGet($http, 'customers/students', cbFn);
    },

    getTeacher: function (id, cbFn) {
      invokeGet($http, 'customers/teacher/' + id, cbFn);
    }
  };
}]);
