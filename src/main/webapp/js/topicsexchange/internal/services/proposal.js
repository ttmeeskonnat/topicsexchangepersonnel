'use strict';

personnelModule.factory("proposal", ['$http', function ($http) {
  return {
    getProposals: function (cbFn) {
      invokeGet($http, 'proposals', cbFn);
    },

    assignAsMentor: function (id, cbFn) {
      invokeGet($http, 'mentorship/assign/proposal/' + id, cbFn);
    },

    getProposal: function (id, cbFn) {
      invokeGet($http, 'proposals/get/' + id, cbFn);
    },

    addComment: function (proposal, commentText, cbFn) {
      invokePost($http, 'proposals/' + proposal.id + '/comment', { text: commentText }, cbFn);
    },

    saveProposal: function (proposal, cbFn) {
      invokePost($http, 'proposals/update/' + proposal.id, proposal, cbFn);
    },

    addProposal: function (proposal, cbFn) {
      invokePost($http, 'proposals/add', proposal, cbFn);
    },

    deleteProposal: function (proposal, cbFn) {
      invokeDelete($http, 'proposals/archive/' + proposal.id, cbFn);
    }
  };
}]);
