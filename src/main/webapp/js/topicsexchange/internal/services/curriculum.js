personnelModule.factory("curriculum", ['$http', function ($http) {
  'use strict';

  return {
    getFaculties: function (cbFn) {
      invokeGet($http, 'curriculum/faculties', cbFn);
    },

    getCurriculums: function (facultyId, cbFn) {
      invokeGet($http, 'curriculum/by_faculty/' + facultyId, cbFn);
    },

    getCurriculumsForCustomer: function (customerId, cbFn) {
      invokeGet($http, 'curriculum/by_customer/' + customerId, cbFn);
    },

    getCurriculum: function (curriculumId, cbFn) {
      invokeGet($http, 'curriculum/' + curriculumId, cbFn);
    }
  };
}]);
