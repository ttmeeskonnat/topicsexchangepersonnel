personnelModule.factory("review", ['$http', function ($http) {
    'use strict';

    return {
        getReview: function (id, cbFn) {
            invokeGet($http, 'review/' + id, cbFn);
        },

        addReview: function (workId, title, data, cbFn) {
            var url = '/reviews/add/' + workId;
            var fd = new FormData();
            fd.append('title', title);
            fd.append('file', data);
            postMultipart($http, url, fd, cbFn);
        },

        updateReview: function (id, title, cbFn) {
            invokePost($http, 'reviews/update/' + id, {title: title}, cbFn);
        },

        storeFile: function (id, data, cbFn) {
            var url = 'reviews/file/' + id;
            var fd = new FormData();
            fd.append('file', data);
            postMultipart($http, url, fd, cbFn);
        },

        removeFile: function (id, cbFn) {
            invokeDelete($http, 'reviews/file/' + id, cbFn)
        }
    };
}]);
