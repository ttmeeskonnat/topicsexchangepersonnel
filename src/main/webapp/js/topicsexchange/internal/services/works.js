personnelModule.factory("works", ['$http', function ($http) {
  'use strict';

  return {
    getAll: function (cbFn) {
      invokeGet($http, 'work/all', cbFn);
    },

    getWork: function (id, cbFn) {
      invokeGet($http, 'work/' + id, cbFn);
    },

    getReviews: function (id, cbFn) {
      invokeGet($http, 'reviews/find/' + id, cbFn);
    }
  };
}]);
