'use strict';

personnelModule.factory("assignment", ['$http', function ($http) {
  return {
    getAssignments: function (cbFn) {
      invokeGet($http, 'mentorship/assignments', cbFn);
    },

    getAssignment: function (id, cbFn) {
      invokeGet($http, 'mentorship/assignment/' + id, cbFn);
    },

    teachersForMentorship: function (cbFn) {
      invokeGet($http, 'customers/teachers_for_mentorship', cbFn);
    },

    reassignMentorship: function (mentorshipId, teacherId, cbFn) {
      invokeGet($http, 'mentorship/reassign/' + mentorshipId + '/' + teacherId, cbFn);
    }
  };
}]);
