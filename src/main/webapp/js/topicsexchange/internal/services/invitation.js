'use strict';

personnelModule.factory("invitation", ['$http', function ($http) {

  return {
    getInvitations: function (cbFn) {
      invokeGet($http, 'mentorship/invitations', cbFn);
    },

    getInvitation: function (id, cbFn) {
      invokeGet($http, 'mentorship/invitation/' + id, cbFn);
    },

    acceptInvitation: function (id, cbFn) {
      invokeGet($http, 'mentorship/accept/' + id, cbFn);
    },

    declineInvitation: function (id, cbFn) {
      invokeGet($http, 'mentorship/decline/' + id, cbFn);
    }
  };
}]);
