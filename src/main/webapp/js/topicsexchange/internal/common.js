function showErrorMessage(errorMessage) {
  var message = $('#errorMessage');
  message.text(errorMessage);
  message.show();
}

function hideErrorMessage() {
  var message = $('#errorMessage');
  message.text('');
  message.hide();
}

function handleResponse(data, callback) {
  var json = null;
  try {
    json = JSON.parse(data);
  }
  catch (Exception) {
    json = data;
  }

  if (json.userError) {
    showErrorMessage(json.userError);
  }
  else {
    hideErrorMessage();
    callback(data);
  }
}

function go(url) {
  hideErrorMessage();
  window.location.href = url;
}

function invokeGet ($http, url, callback) {
  $http.get(url).
    success(function (data, status, headers, config) {
      handleResponse(data, callback);
    }).
    error(function (data, status, headers, config) {
      showErrorMessage('Technical error');
    }
  );
}

function invokePost($http, url, postedData, callback) {
  $http.post(url, postedData).
    success(function (data, status, headers, config) {
      handleResponse(data, callback);
    }).
    error(function (data, status, headers, config) {
      showErrorMessage('Technical error');
    }
  );
}

function postMultipart($http, url, fd, cbFn) {
  $http.post(url, fd, {
    transformRequest: angular.identity,
    headers: {
      'Content-Type': undefined
    }
  }).success(function (data, status, headers, config) {
    handleResponse(data, cbFn);
  }).error(function (data, status, headers, config) {
    showErrorMessage('Technical error');
  });
}

function invokeDelete($http, url, callback) {
  $http.delete(url).
    success(function (data, status, headers, config) {
      handleResponse(data, callback);
    }).
    error(function (data, status, headers, config) {
      showErrorMessage('Technical error');
    }
  );
}

$(document).ready(function () {
  //When page loads...
  $(".tab_content").hide(); //Hide all content
  $("ul.tabs li:first").addClass("active").show(); //Activate first tab
  $(".tab_content:first").show(); //Show first tab content

  //On Click Event
  $("ul.tabs li").click(function () {

    $("ul.tabs li").removeClass("active"); //Remove any "active" class
    $(this).addClass("active"); //Add "active" class to selected tab
    $(".tab_content").hide(); //Hide all tab content

    var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
    $(activeTab).fadeIn(); //Fade in the active ID content
    return false;
  });
});
