'use strict';

personnelModule.directive('pagination', function() {
 return {
   restrict: 'E',
   scope: {
     filtered: '@filtered'
   },
   replace: true,
   transclude: false,
   link: function(scope, element, attrs) {
   },
   controller: function($rootScope, $scope, $element, $attrs){

     var table_id = $attrs.table;

     $scope.prevPage = function() {
       if ($rootScope.currentPage > 0) {
         $rootScope.currentPage--;
       }
     };

     $scope.prevPageDisabled = function() {
       return $rootScope.currentPage === 0 ? "disabled" : "";
     };

     $scope.pageCount = function() {
       return Math.ceil($scope.filtered/$rootScope.itemsPerPage)-1;
     };

     $scope.nextPage = function() {
       if ($rootScope.currentPage < $scope.pageCount()) {
         $rootScope.currentPage++;
       }
     };

     $scope.nextPageDisabled = function() {
       return $rootScope.currentPage === $scope.pageCount() ? "disabled" : "";
     };

     $scope.range = function(){
       var range = [];
       for (var i=0; i<=$scope.pageCount(); i++) {
         range.push(i);
       }
       if($("#" + table_id + ":visible > tbody > tr").length == 0){
         $scope.setPage(0);
       }
       return range;
     };

     $scope.setPage = function(n) {
       $rootScope.currentPage = n;
     };

     $scope.isActive = function(n){
       return ($rootScope.currentPage) == n ? "active" : "";
     };

     $scope.showPagination = function(){
       return $scope.filtered > $rootScope.itemsPerPage;
     }
   },
   template: "<div ng-show='showPagination()' class='pagination'>" +
             " <ul>" +
             "  <li class='page' ng-class='prevPageDisabled()'>" +
             "   <a href ng-click='prevPage()'>< Prev</a>" +
             "  </li>" +
             "  <li ng-class='isActive(n)' class='page' ng-repeat='n in range()'" +
             "     ng-click='setPage(n)'>" +
             "   <a>{{n+1}}</a>" +
             "  </li>" +
             "  <li class='page' ng-class='nextPageDisabled()'>" +
             "   <a href ng-click='nextPage()'>Next ></a>" +
             "  </li>" +
             " </ul>" +
             "</div>"
 };
});