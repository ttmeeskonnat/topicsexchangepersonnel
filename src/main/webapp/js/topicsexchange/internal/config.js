'use strict';

var contextPath,
    personnelModule = angular.module('personnel', ['ngRoute','textAngular', 'angular-table'], function($httpProvider) {
      $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
      $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

      $(document).ready(function () {
          contextPath = $('#requestContextPath').val();
          $(".tablesorter").tablesorter();
        }
      );

      var param = function(obj) {
          var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

          for(name in obj) {
            value = obj[name];

            if(value instanceof Array) {
              for(i=0; i<value.length; ++i) {
                subValue = value[i];
                fullSubName = name + '[' + i + ']';
                innerObj = {};
                innerObj[fullSubName] = subValue;
                query += param(innerObj) + '&';
              }
            }
            else if(value instanceof Object) {
              for(subName in value) {
                subValue = value[subName];
                fullSubName = name + '[' + subName + ']';
                innerObj = {};
                innerObj[fullSubName] = subValue;
                query += param(innerObj) + '&';
              }
            }
            else if(value !== undefined && value !== null)
              query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
          }

          return query.length ? query.substr(0, query.length - 1) : query;
      };

      // Override $http service's default transformRequest
      $httpProvider.defaults.transformRequest = [function(data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
      }];
    })
      .constant('_', window._)
      // use in views, ng-repeat="x in _.range(3)"
      .run(['$rootScope', function ($rootScope) {
        $rootScope._ = window._;
      }]);

personnelModule.config(['$routeProvider',
 function($routeProvider) {
   $routeProvider.
     when('/mentorship/invitations', {
        templateUrl: contextPath + '/js/topicsexchange/internal/views/invitation/list.html',
        controller: 'InvitationListCtrl'
     }).
     when('/mentorship/assignments', {
        templateUrl: contextPath + '/js/topicsexchange/internal/views/assignment/list.html',
        controller: 'AssignmentListCtrl'
     }).
     when('/work/all',{
       templateUrl: contextPath + '/js/topicsexchange/internal/views/work/works_list.html',
       controller: 'WorkListCtrl'
     }).
     when('/curriculum/teachers',{
       templateUrl: contextPath + '/js/topicsexchange/internal/views/faculty/teachers.html',
       controller: 'TeachersListCtrl'
     }).
     when('/curriculum/teacher/:teacherId',{
       templateUrl: contextPath + '/js/topicsexchange/internal/views/faculty/teacher.html',
       controller: 'TeacherDetailsCtrl'
     }).
     when('/curriculum/students',{
       templateUrl: contextPath + '/js/topicsexchange/internal/views/faculty/students.html',
       controller: 'StudentsListCtrl'
     }).
     when('/curriculum/faculties',{
       templateUrl: contextPath + '/js/topicsexchange/internal/views/faculty/faculties.html',
       controller: 'FacultiesListCtrl'
     }).
     when('/curriculum/by_faculty/:facultyId',{
       templateUrl: contextPath + '/js/topicsexchange/internal/views/faculty/curriculums.html',
       controller: 'CurriculumsListCtrl'
     }).
     when('/curriculum/:curriculumId',{
       templateUrl: contextPath + '/js/topicsexchange/internal/views/faculty/curriculum.html',
       controller: 'CurriculumDetailsCtrl'
     }).
     when('/proposals', {
       templateUrl: contextPath + '/js/topicsexchange/internal/views/proposal/list.html',
       controller: 'ProposalListCtrl'
     }).
     when('/proposals/:proposalId', {
       templateUrl: contextPath + '/js/topicsexchange/internal/views/proposal/detail.html',
       controller: 'ProposalDetailCtrl'
     }).
     when('/proposals/add/new', {
       templateUrl: contextPath + '/js/topicsexchange/internal/views/proposal/add.html',
       controller: 'ProposalAddCtrl'
     }).
     when('/mentorship/invitation/:invitationId', {
       templateUrl: contextPath + '/js/topicsexchange/internal/views/invitation/detail.html',
       controller: 'InvitationDetailCtrl'
     }).
     when('/mentorship/assignment/:assignmentId', {
       templateUrl: contextPath + '/js/topicsexchange/internal/views/assignment/detail.html',
       controller: 'AssignmentDetailCtrl'
     }).
     otherwise({
       templateUrl: contextPath + '/js/topicsexchange/internal/views/dashboard.html',
       controller: 'MainCtrl'
     });
 }]);

personnelModule.filter('offset',
function() {
 return function(input, start) {
   start = parseInt(start, 10);
   return input.slice(start);
 };
});