'use strict';

personnelModule.controller('AssignmentDetailCtrl', ['$rootScope', '$scope', '$window', 'assignment', '$routeParams',
  function ($rootScope, $scope, $window, assignment, $routeParams) {

    $rootScope.models = {
      viewName: 'Assignment details'
    };

    $rootScope.models = {
      assignment: {},
      teachers: {}
    };

    assignment.getAssignment($routeParams.assignmentId, function (data) {
      $scope.models.assignment = data;

      assignment.teachersForMentorship(function (data) {
        $scope.models.teachers = data;
      });
    });

    $scope.methods = {
      backToAssignments: function () {
        backToAssignments()
      },

      reassign: function (mentorshipId, teacherId) {
        assignment.reassignMentorship(mentorshipId, teacherId);
      }
    };



    function backToAssignments() {
      $window.location.href = '#/mentorship/assignments';
    }
  }]);
