personnelModule.controller('AssignmentListCtrl', [
    '$rootScope', '$scope', '$window', 'assignment', function ($rootScope, $scope, $window, assignment) {
        'use strict';

        $rootScope.models = {
          viewName: 'Assignments'
        };

        $scope.models = {
            assignments: []
        };

        assignment.getAssignments(function(data) {
            $scope.models.assignments = data;
        });

        $scope.methods = {
          showAssignment: function (id) {
            assignment.getAssignment(id, function () {
              $window.location.href = '#/mentorship/assignment/' + id;
            });
          }
        }
}]);
