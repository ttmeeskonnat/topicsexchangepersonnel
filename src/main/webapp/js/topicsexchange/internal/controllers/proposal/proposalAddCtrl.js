'use strict';

personnelModule.controller('ProposalAddCtrl', ['$rootScope', '$scope', '$window', 'proposal', '$routeParams',
    function ($rootScope, $scope, $window, proposal, $routeParams) {

    $rootScope.models = {
      viewName: 'Add Proposal'
    };

    $rootScope.models = {
      proposal: {}
    };

    $scope.methods = {
      addProposal: function (proposalObject) {
        proposal.addProposal(proposalObject, function (data) {
          backToProposals();
        });
      },

      backToProposals: function () {
        backToProposals();
      }
    };

    function backToProposals() {
      $window.location.href = '#/proposals';
    }
  }]);
