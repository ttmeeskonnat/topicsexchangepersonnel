'use strict';

personnelModule.controller('ProposalListCtrl', [
  '$rootScope', '$scope', '$window', 'proposal', function ($rootScope, $scope, $window, proposal) {

    $rootScope.models = {
      viewName: 'Proposals'
    }

    $scope.models = {
      proposals: []
    }

    proposal.getProposals(function (data) {
      $scope.models.proposals = data;
    });

    $scope.methods = {
      isMentor: function (proposal) {
        if (proposal.mentors) {
          for (var i = 0; i < proposal.mentors.length; i++) {
            var mentor = proposal.mentors[i];
            if (mentor.id == $window.userId) {
              return true;
            }
          }
        }
        return false;
      },

      assignAsMentor: function (proposalId, e) {
        proposal.assignAsMentor(proposalId, function (data) {
        });
        e.stopPropagation();
      },

      showProposal: function (id) {
        proposal.getProposal(id, function () {
          $window.location.href = '#/proposals/' + id;
        });
      },

      deleteProposal: function (proposalId, e) {
        proposal.deleteProposal(proposalId, function (data) {
          // todo: reload proposals list
          $scope.models.proposals = _.without(
            $scope.models.proposals, _.findWhere($scope.models.proposals, {id: proposalId}));
        });
        e.stopPropagation();
      }
    }
  }]);
