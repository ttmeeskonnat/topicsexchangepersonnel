'use strict';

personnelModule.controller('ProposalDetailCtrl', ['$rootScope', '$scope', '$window', 'proposal', '$routeParams', '$sce',
  function ($rootScope, $scope, $window, proposal, $routeParams,  $sce) {

    $rootScope.models = {
      viewName: 'Proposal details'
    };

    $rootScope.models = {
      proposal: {},
      isEditAllowed: {}
    };

    proposal.getProposal($routeParams.proposalId, function (data) {
      $rootScope.models.isEditAllowed = ($window.userId == data.author.id);
      $scope.models.proposal = data;
      $scope.models.comments = $scope.models.proposal.comments;
    });

    $scope.showPopup = false;

    $scope.methods = {
      showComments: function() {
        if(!_.isUndefined($scope.models.comments)){
          return $scope.models.comments.length > 0;
        }
        return false;
      },

      showPopup: function () {
        $scope.showPopup = true;
      },

      hidePopup: function () {
        $scope.showPopup = false;
      },

      addComment: function (text) {
        console.log("Comment: " + text);

        if(!_.isUndefined(text) && text != ""){
          proposal.addComment($scope.models.proposal, text, function(comment){
            $scope.methods.hidePopup();
            console.log(comment);
            $scope.models.comments.push(comment);
          });
        }
      },

      saveProposal: function (proposalObject) {
        proposal.saveProposal(proposalObject, function (data) {
          backToProposals();
        });
      },

      removeProposal: function (proposalObject) {
        proposal.removeProposal(proposalObject, function (data) {
          backToProposals();
        });
      },

      backToProposals: function () {
        backToProposals();
      }
    };

    $scope.renderHtml = function(html_code) {
      return $sce.trustAsHtml(html_code);
    };

    function backToProposals() {
      $window.location.href = '#/proposals';
    }

  }]);
