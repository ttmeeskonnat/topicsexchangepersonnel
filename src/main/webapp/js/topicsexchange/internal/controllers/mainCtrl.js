personnelModule.controller('MainCtrl', [
    '$rootScope', '$scope', 'proposal', function ($rootScope, $scope, proposal) {

    var teacher = 'TEACHER',
        reviewer = 'REVIEWER',
        curriculum_manager = 'CURRICULUM_MANAGER',
        roles = window.userRolers || [];

    $rootScope.itemsPerPage = 6;
    $rootScope.currentPage = 0;

    $rootScope.models = {
        viewName: ''
    };

    $scope.showStatus = function(status){
        var statuses = {
          'IN_PROGRESS':'In progress',
          'FINISHED':'Finished',
          'ARCHIVED':'Archived',
          'ACTIVE':'Active'
        };
        return statuses[status];
    };

    $scope.type = {
      THESIS: true,
      PRACTICE: true
    };

    $scope.checkedType = function (val) {
      return $scope.type[val.type];
    };

    $scope.count = function(arr){
      if(arr == null || arr == undefined)
        return 0;

        return arr.length
    };

    $scope.shortText = function(str){
      if(str != null && str != undefined && str.length > 100) str = str.substring(0,100);
      return str;
    };

    $scope.methods = {
      getActiveUserName: function(){
        return window.userName;
      },
      isTeacher: function(){
        return _.contains(roles, teacher);
      },
      isReviewer: function() {
        return _.contains(roles, reviewer)
      },
      isCurriculumManager: function() {
        return _.contains(roles, curriculum_manager)
      }
    }
}]);