personnelModule.controller('WorkListCtrl', [
  '$rootScope', '$scope', '$window', 'works', function ($rootScope, $scope, $window, works) {
    'use strict';

    function removeFromList(id) {
      $scope.models.works = _.without(
        $scope.models.works, _.findWhere($scope.models.works, {id: id})
      );
    }

    $rootScope.models = {
      viewName: 'Works'
    };

    $scope.models = {
      works: []
    };

    works.getAll(function (data) {
      $scope.models.works = data;
    });

    $scope.methods = {}
  }]);
