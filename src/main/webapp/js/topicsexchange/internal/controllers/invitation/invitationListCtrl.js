'use strict';

personnelModule.controller('InvitationListCtrl', [
  '$rootScope', '$scope', '$window', 'invitation', function ($rootScope, $scope, $window, invitation) {

    function removeFromList(id) {
      $scope.models.invitations = _.without(
        $scope.models.invitations, _.findWhere($scope.models.invitations, {id: id})
      );
    }

    $rootScope.models = {
      viewName: 'Invitations'
    };

    $scope.models = {
      invitations: []
    };

    invitation.getInvitations(function (data) {
      $scope.models.invitations = data;
    });

    $scope.methods = {
      acceptInvitation: function (id, e) {
        invitation.acceptInvitation(id, function (data) {
          removeFromList(id);
        });
        e.stopPropagation();
      },

      declineInvitation: function (id, e) {
        invitation.declineInvitation(id, function (data) {
          removeFromList(id);
        });
        e.stopPropagation();
      },

      showInvitation: function (id) {
        invitation.getInvitation(id, function () {
          $window.location.href = '#/mentorship/invitation/' + id;
        });
      }
    }
  }]);
