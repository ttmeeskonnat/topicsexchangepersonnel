'use strict';

personnelModule.controller('InvitationDetailCtrl', ['$rootScope', '$scope', '$window', 'invitation', '$routeParams',
  function ($rootScope, $scope, $window, invitation, $routeParams) {

    $rootScope.models = {
      viewName: 'Invitation details'
    };

    $rootScope.models = {
      invitation: {}
    };

    invitation.getInvitation($routeParams.invitationId, function (data) {
      $scope.models.invitation = data;
    });

    $scope.methods = {
      acceptInvitation: function (id) {
        invitation.acceptInvitation(id, function (data) {
          backToInvitations();
        });
      },

      declineInvitation: function (id) {
        invitation.declineInvitation(id, function (data) {
          backToInvitations();
        });
      },

      backToInvitations: function () {
        backToInvitations()
      }
    };

    function backToInvitations() {
      $window.location.href = '#/mentorship/invitations';
    }
  }]);
