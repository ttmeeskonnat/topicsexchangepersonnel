'use strict';

personnelModule.controller('StudentsListCtrl', [
  '$rootScope', '$scope', '$window', 'customer', function ($rootScope, $scope, $window, customer) {

    $rootScope.models = {
      viewName: 'Students'
    };

    $scope.models = {
      students: []
    };

    customer.getStudents(function (data) {
      $scope.models.students = data;
    });

    $scope.methods = {
    }
  }
]);
