'use strict';

personnelModule.controller('TeachersListCtrl', [
  '$rootScope', '$scope', '$window', 'customer', function ($rootScope, $scope, $window, customer) {

    $rootScope.models = {
      viewName: 'Teachers'
    };

    $scope.config = {
      itemsPerPage: 10,
      fillLastPage: true
    };

    $scope.models = {
      teachers: []
    };

    customer.getTeachers(function (data) {
      $scope.models.teachers = data;
    });

    $scope.methods = {
      showTeacher: function (teacherId) {
        customer.getTeacher(teacherId, function () {
          $window.location.href = '#/curriculum/teacher/' + teacherId;
        });
      }
    }
  }
]);
