'use strict';

personnelModule.controller('TeacherDetailsCtrl', [
  '$rootScope', '$scope', '$window', 'customer', 'curriculum', '$routeParams',
  function ($rootScope, $scope, $window, customer, curriculum, $routeParams) {

    $rootScope.models = {
      viewName: 'Teacher'
    };

    $scope.models = {
      teacher: [],
      curriculums: []
    };

    customer.getTeacher($routeParams.teacherId, function (data) {
      $scope.models.teacher = data;
      readCurriculums($scope.models.teacher.id);
    });

    function readCurriculums(teacherId) {
      curriculum.getCurriculumsForCustomer(teacherId, function(data) {
        $scope.models.curriculums = data;
      })
    }

    $scope.methods = {
      showCurriculum: function (curriculumId) {
        curriculum.getCurriculum(curriculumId, function () {
          $window.location.href = '#/curriculum/' + curriculumId;
        });
      }
    }
  }
]);
