personnelModule.controller('CurriculumDetailsCtrl', [
  '$rootScope', '$scope', '$window', 'curriculum','$routeParams',
    function ($rootScope, $scope, $window, curriculum, $routeParams) {
    'use strict';

    $rootScope.models = {
      viewName: 'Curriculum'
    };

    $scope.models = {
      curriculum: []
    };

    curriculum.getCurriculum($routeParams.curriculumId, function(data) {
      $scope.models.curriculum = data;
    });

    $scope.methods = {

    }
  }]
);
