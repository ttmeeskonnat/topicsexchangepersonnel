personnelModule.controller('CurriculumsListCtrl', [
  '$rootScope', '$scope', '$window', 'curriculum','$routeParams',
    function ($rootScope, $scope, $window, curriculum, $routeParams) {
    'use strict';

    $rootScope.models = {
      viewName: 'Curriculum'
    };

    $scope.models = {
      curriculums: []
    };

    curriculum.getCurriculums($routeParams.facultyId, function(data) {
      $scope.models.curriculums = data;
    });

    $scope.methods = {

    }
  }]
);
