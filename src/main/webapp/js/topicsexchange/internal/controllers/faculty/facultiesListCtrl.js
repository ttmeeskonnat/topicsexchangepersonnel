personnelModule.controller('FacultiesListCtrl', [
  '$rootScope', '$scope', '$window', 'curriculum', function ($rootScope, $scope, $window, curriculum) {
    'use strict';

    function removeFromList(id) {
      $scope.models.faculties = _.without(
        $scope.models.faculties, _.findWhere($scope.models.faculties, {id: id})
      );
    }

    $rootScope.models = {
      viewName: 'Faculties'
    };

    $scope.models = {
      faculties: []
    };

    curriculum.getFaculties(function (data) {
      $scope.models.faculties = data;
    });

    $scope.methods = {
      showCurriculums: function (facultyId) {
        curriculum.getCurriculums(facultyId, function () {
          $window.location.href = '#/curriculum/by_faculty/' + facultyId;
        });
      }
    }
  }
]);
