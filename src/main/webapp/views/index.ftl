<!DOCTYPE html>
<html ng-app="personnel">
<head>
  <meta charset="utf-8"/>
  <title>Topics exchange personnel.</title>

  <link rel="stylesheet" href="${request.contextPath}/css/layout.css" type="text/css" media="screen"/>
  <!--[if lt IE 9]>
  <link rel="stylesheet" href="${request.contextPath}/css/ie.css" type="text/css" media="screen"/>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
  <link rel="stylesheet" href="http://textangular.com/dist/textAngular.css">

  <script src="${request.contextPath}/js/topicsexchange/external/jquery-1.5.2.min.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/external/hideshow.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/external/jquery.tablesorter.min.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/external/jquery.equalHeight.js" type="text/javascript"></script>

  <script src="${request.contextPath}/js/topicsexchange/external/angular.3.14.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/external/angular-route.3.14.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/external/underscore_1.8.2.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/external/angular-table.min.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/external/angular-tabs.min.js" type="text/javascript" ></script>

  <script src="${request.contextPath}/js/topicsexchange/external/rangy-core.js"></script>
  <script src="${request.contextPath}/js/topicsexchange/external/textAngular-rangy.min.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/external/textAngular-sanitize.min.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/external/textAngular.min.js" type="text/javascript"></script>

  <script src="${request.contextPath}/js/topicsexchange/internal/config.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/common.js" type="text/javascript"></script>

  <!-- services -->
  <script src="${request.contextPath}/js/topicsexchange/internal/services/proposal.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/services/works.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/services/invitation.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/services/assignment.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/services/curriculum.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/services/customer.js" type="text/javascript"></script>

  <!-- controllers -->
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/mainCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/proposal/proposalListCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/proposal/proposalAddCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/proposal/proposalDetailCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/invitation/invitationListCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/invitation/invitationDetailCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/assignment/assignmentListCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/assignment/assignmentDetailCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/work/workListCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/faculty/facultiesListCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/faculty/curriculumsListCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/faculty/curriculumDetailsCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/faculty/teachersListCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/faculty/studentsListCtrl.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/controllers/faculty/teacherDetailsCtrl.js" type="text/javascript"></script>

  <!-- directives -->
  <script src="${request.contextPath}/js/topicsexchange/internal/directives/popup.js" type="text/javascript"></script>
  <script src="${request.contextPath}/js/topicsexchange/internal/directives/pagination.js" type="text/javascript"></script>

  <input type="hidden" id="requestContextPath" name="requestContextPath" value="${request.contextPath}"/>

  <script type="text/javascript">
    $(function () {
      $('.column').equalHeight();
    });

    window.userId = ${userId};
    window.userRolers = ${userRoles};
    window.userName = "${username}";
  </script>

  <style>
    .pagination {
      display: inline-block;
      padding-left: 0;
      margin: 0px 0;
      border-radius: 4px;
    }

    .pagination ul {
      padding: 0 7px;
    }

    .btn {
        display: inline-block;
        padding: 10px;
        border-radius: 5px; /*optional*/
        color: #aaa;
        font-size: .875em;
    }

    .page {
        display: inline-block;
        padding: 0px 9px;
        margin-right: 4px;
        border-radius: 3px;
        border: solid 1px #c0c0c0;
        background: #e9e9e9;
        box-shadow: inset 0px 1px 0px rgba(255,255,255, .8), 0px 1px 3px rgba(0,0,0, .1);
        font-size: .875em;
        font-weight: bold;
        text-decoration: none;
        color: #717171;
        text-shadow: 0px 1px 0px rgba(255,255,255, 1);
    }

    .page.active {
        background: #616161;
        box-shadow: inset 0px 0px 8px rgba(0,0,0, .5), 0px 1px 0px rgba(255,255,255, .8);
        color: #f0f0f0;
        text-shadow: 0px 0px 3px rgba(0,0,0, .5);
    }

    .page.disabled,
    .page.disabled a{
        color: #999;
        cursor: default;
        text-decoration: none;
    }


  </style>
</head>
<body ng-controller="MainCtrl">

<header id="header">
  <hgroup>
    <h1 class="site_title" style="overflow: hidden; white-space: nowrap;"><a href="">University</a></h1>
    <h2 class="section_title">Dashboard</h2>
  </hgroup>
</header>
<!-- end of header bar -->

<section id="secondary_bar">
  <div class="user">
    <p>${username} (<a href="#">5 Messages</a>)</p>
    <form>
      <a class="logout_user" href="${request.contextPath}/logout" title="Logout">Logout</a>
    </form>
  </div>
  <div class="breadcrumbs_container">
    <article class="breadcrumbs"><a href="${request.contextPath}/dashboard">Dashboard</a>

      <div class="breadcrumb_divider"></div>
      <a class="current">{{models.viewName}}</a></article>
  </div>
</section>
<!-- end of secondary bar -->

<aside id="sidebar" style="overflow: hidden; height: auto;">
  <form class="quick_search">
    <input type="text" value="Quick Search" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
  </form>
  <hr/>
  <div ng-show="methods.isTeacher()">
    <h3>Proposal</h3>
    <ul class="toggle">
      <li class="icn_categories"><a href="javascript:go('#/proposals')">Show All</a></li>
      <li class="icn_new_article"><a href="javascript:go('#/proposals/add/new')">Add Proposal</a></li>
    </ul>
  </div>

  <div ng-show="methods.isTeacher()">
    <h3>Mentorship</h3>
    <ul class="toggle">
      <li class="icn_categories"><a href="javascript:go('#/mentorship/invitations')">Show Invitations</a></li>
      <li class="icn_categories"><a href="javascript:go('#/mentorship/assignments')">Show Assignments</a></li>
    </ul>
  </div>

  <div ng-show="methods.isCurriculumManager()">
    <h3>Curriculum</h3>
    <ul class="toggle">
      <li class="icn_categories"><a href="javascript:go('#/curriculum/faculties')">Show Faculties</a></li>
      <li class="icn_view_users"><a href="javascript:go('#/curriculum/teachers')">Show Teachers</a></li>
      <li class="icn_view_users"><a href="javascript:go('#/curriculum/students')">Show Students</a></li>
    </ul>
  </div>

  <div ng-show="methods.isReviewer()">
    <h3>Reviews</h3>
    <ul class="toggle">
      <li class="icn_categories"><a href="javascript:go('#/work/all')">Show works</a></li>
    </ul>
  </div>

  <footer style="margin-top:20px">
    <hr/>
    <p><strong>Copyright &copy; 2015 Topics Exchange</strong></p>
    <p>Theme by <a href="http://www.medialoot.com">MediaLoot</a></p>
  </footer>
</aside>
<!-- end of sidebar -->

<section id="main" style="overflow: hidden; height: auto;">
  <div id="errorMessage" class="clear error" style="display:none">Error</div>
  <div ng-view class="view-frame"></div>
  <div class="spacer"></div>
</section>

</body>
</html>