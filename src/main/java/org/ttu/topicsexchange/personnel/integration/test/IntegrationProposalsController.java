package org.ttu.topicsexchange.personnel.integration.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.ttu.topicsexchange.personnel.mocks.TestProposalsFeeder;
import org.ttu.topicsexchange.personnel.model.Proposal;

import java.util.Collection;
import java.util.Optional;

import static java.util.stream.Collectors.toSet;
import static org.ttu.topicsexchange.personnel.model.Proposal.Status.ACTIVE;

/**
 * Controller to test integration
 */
@Controller
@RequestMapping("/integration/proposals")
public class IntegrationProposalsController {

  private static final String PROPOSAL_ID = "proposalId";

  @Autowired
  TestProposalsFeeder testProposalsFeeder;

  @RequestMapping(value="all", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Proposal> findAll() {
    return testProposalsFeeder.getProposals()
      .stream()
      .filter(el -> el.getStatus().equals(ACTIVE))
      .collect(toSet());
  }

  @RequestMapping(value="/get/{proposalId}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Proposal get(@PathVariable(PROPOSAL_ID) Long proposalId) {
    Optional<Proposal> proposal = findAll()
      .stream()
      .filter(el -> el.getId().equals(proposalId))
      .findFirst();
    return proposal.isPresent() ? proposal.get() : null;
  }
}
