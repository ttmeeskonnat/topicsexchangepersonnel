package org.ttu.topicsexchange.personnel.integration;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Map;

@Component
public class ServiceInvoker implements InitializingBean {

  @Value("${service.layer.base.url}")
  String baseUrl;

  @Autowired
  @Qualifier("serviceLayerTemplate")
  RestTemplate restTemplate;

  @Autowired
  Jackson2ObjectMapperFactoryBean objectMapperFactory;

  @Override
  public void afterPropertiesSet() throws Exception {
    MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    converter.setObjectMapper(objectMapperFactory.getObject());
    restTemplate.setMessageConverters(Arrays.asList(converter));
  }


  public <T> T get(String path, Map<String, ?> urlVariables, Class<T> responseType) {
    return restTemplate.getForObject(baseUrl + path, responseType, urlVariables);
  }

  public void put(String path, Object object, Map<String, ?> urlVariables) {
    restTemplate.put(baseUrl + path, object, urlVariables);
  }

  public <T> T post(String path, Object object, Map<String, ?> urlVariables, Class<T> responseType) {
    return restTemplate.postForObject(baseUrl + path, object, responseType, urlVariables);
  }

  public void delete(String path, Map<String, ?> urlVariables) {
    restTemplate.delete(baseUrl + path, urlVariables);
  }

}
