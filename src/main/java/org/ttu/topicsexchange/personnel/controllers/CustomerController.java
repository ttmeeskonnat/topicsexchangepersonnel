package org.ttu.topicsexchange.personnel.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.ttu.topicsexchange.personnel.model.customers.*;
import org.ttu.topicsexchange.personnel.services.CustomerComponent;

import java.util.Collection;

@Controller
@RequestMapping("/customers")
public class CustomerController {
  private static final String CUSTOMER_ID = "id";

  @Autowired
  CustomerComponent customerComponent;

  @RequestMapping(value="/teacher/{id}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Teacher getTeacher(@PathVariable(CUSTOMER_ID) String id){
    return customerComponent.findTeacherById(Long.parseLong(id));
  }

  @RequestMapping(value="/update/current", method = RequestMethod.POST, produces = "application/json")
  public @ResponseBody boolean updateCurrentUser(
    @RequestParam String firstName, @RequestParam String lastName,
    @RequestParam String phone, @RequestParam String email) {

    updatePerson(ControllerSupport.getCurrentPerson(), firstName, lastName, phone, email);

    return true;
  }

  @RequestMapping(value="/update/person", method = RequestMethod.POST, produces = "application/json")
  public @ResponseBody boolean updatePerson(
    @RequestParam String id, @RequestParam String firstName, @RequestParam String lastName,
    @RequestParam String phone, @RequestParam String email) {

    Person person = customerComponent.findPersonById(Long.parseLong(id));
    updatePerson(person, firstName, lastName, phone, email);
    return true;
  }

  @RequestMapping(value="/update/company", method = RequestMethod.POST, produces = "application/json")
  public @ResponseBody boolean updateCompany(
    @RequestParam String id, @RequestParam String companyName, @RequestParam String email) {

    Company company = customerComponent.findCompanyById(Long.parseLong(id));
    company.setName(companyName);
    company.setEmail(email);
    return true;
  }

  @RequestMapping(value="", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  Collection<Customer> getAll() {
    return customerComponent.getAllCustomers();
  }

  @RequestMapping(value="/students/{name}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  Collection<Student> findStudentByName(@PathVariable("name") String name) {
    return customerComponent.findStudentsByName(name);
  }

  @RequestMapping(value="/teachers/{name}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  Collection<Teacher> findTeacherByName(@PathVariable("name") String name) {
    return customerComponent.findTeachersByName(name);
  }

  @RequestMapping(value="/company/{name}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody
  Collection<Company> findCompanyByName(@PathVariable("name") String name) {
    return customerComponent.findCompanyByName(name);
  }

  @RequestMapping(value="/students", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Student> getStudents() {
    return customerComponent.getStudents();
  }

  @RequestMapping(value="/companies", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Company> getCompanies() {
    return customerComponent.getCompanies();
  }

  @RequestMapping(value="/teachers", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Teacher> getTeachers() {
    return customerComponent.getTeachers();
  }

  @RequestMapping(value="/teachers_for_mentorship", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Teacher> getTeachersForMentorship() {
    // todo: make additional filter for mentorship count
    return customerComponent.getTeachers();
  }

  private void updatePerson(Person person, String firstName, String lastName, String phone, String email) {
    person.setFirstName(firstName);
    person.setLastName(lastName);
    person.setPhone(phone);
    person.setEmail(email);

    // todo: validate fields
  }
}
