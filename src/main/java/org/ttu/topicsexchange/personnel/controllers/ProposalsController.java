package org.ttu.topicsexchange.personnel.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.ttu.topicsexchange.personnel.model.Comment;
import org.ttu.topicsexchange.personnel.model.Proposal;
import org.ttu.topicsexchange.personnel.services.ProposalComponent;
import org.ttu.topicsexchange.personnel.services.ServiceException;

import java.util.Collection;
import java.util.Date;

import static org.ttu.topicsexchange.personnel.controllers.ControllerSupport.getCurrentPerson;
import static org.ttu.topicsexchange.personnel.mocks.TestDataFeeder.nextId;
import static org.ttu.topicsexchange.personnel.services.ServiceException.Error.PROPOSAL_UPDATING_DENIED;

@Controller
@RequestMapping("/proposals")
public class ProposalsController {

  private static final String PROPOSAL_ID = "proposalId";

  @Autowired
  ProposalComponent proposalComponent;

  @RequestMapping(value="", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Proposal> findAll() {
    return proposalComponent.getActiveProposals();
  }

  @RequestMapping(value="/get/{proposalId}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Proposal get(@PathVariable(PROPOSAL_ID) String proposalId) {
    return proposalComponent.getProposalById(Long.valueOf(proposalId));
  }

  @RequestMapping(value="/{proposalId}/comment", method = RequestMethod.POST, produces = "application/json")
  public @ResponseBody Comment addCommentToProposal(@PathVariable(PROPOSAL_ID) Long proposalId,
                                                 @RequestParam String text){

    Proposal proposal = proposalComponent.getProposalById(proposalId);
    Comment comment = new Comment();
    comment.setId((nextId()));
    comment.setText(text);
    comment.setAuthor(getCurrentPerson());
    proposal.getComments().add(comment);
    proposalComponent.update(proposal);

    return comment;
  }

  @RequestMapping(value="/update/{proposalId}", method = RequestMethod.POST, produces = "application/json")
  public @ResponseBody void updateProposal(@PathVariable(PROPOSAL_ID) Long proposalId,
                                           @RequestParam String title,
                                           @RequestParam String description,
                                           @RequestParam String type) {
    Proposal proposal = proposalComponent.getProposalById(proposalId);
    if (!isAuthor(proposal)) {
      throw new ServiceException(PROPOSAL_UPDATING_DENIED,
        "Updating of proposal with id =" + proposalId + " is denied for customer: " + getCurrentPerson());
    }
    proposal.setTitle(title);
    proposal.setDescription(description);
    proposal.setType(Proposal.Type.valueOf(type));
    proposalComponent.update(proposal);
  }

  @RequestMapping(value="/add", method = RequestMethod.POST, produces = "application/json")
  public @ResponseBody void add(@RequestParam String title, @RequestParam String description, @RequestParam String type) {
    Proposal proposal = new Proposal();
    proposal.setTitle(title);
    proposal.setDescription(description);
    proposal.setType(Proposal.Type.valueOf(type));
    proposal.setStatus(Proposal.Status.ACTIVE);
    proposal.setCreationDate(new Date());
    proposal.setAuthor(getCurrentPerson());

    proposalComponent.add(proposal);
  }

  @RequestMapping(value="/archive/{proposalId}", method = RequestMethod.DELETE, produces = "application/json")
  public @ResponseBody void archive(@PathVariable(PROPOSAL_ID) Long proposalId) {
    Proposal proposal = proposalComponent.getProposalById(proposalId);
    proposalComponent.archive(proposal);
  }

  private boolean isAuthor(Proposal proposal) {
    return proposal != null && proposal.getAuthor().equals(getCurrentPerson());
  }
}
