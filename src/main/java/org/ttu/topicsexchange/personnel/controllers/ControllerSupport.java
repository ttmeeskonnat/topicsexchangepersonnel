package org.ttu.topicsexchange.personnel.controllers;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.ttu.topicsexchange.personnel.authentication.User;
import org.ttu.topicsexchange.personnel.model.customers.Person;

public class ControllerSupport {

  public static Person getCurrentPerson() {
    UsernamePasswordAuthenticationToken principal = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
    return ((User) principal.getPrincipal()).getRelatedPerson();
  }

}
