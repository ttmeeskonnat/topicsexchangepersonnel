package org.ttu.topicsexchange.personnel.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.ttu.topicsexchange.personnel.model.Work;
import org.ttu.topicsexchange.personnel.services.WorkComponent;

import java.util.Collection;

@Controller
@RequestMapping("/work")
public class WorkController {

  @Autowired
  WorkComponent workComponent;

  @RequestMapping(value="/all", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Work> getAll() {
    return workComponent.getAllWorks();
  }

  @RequestMapping(value="/thesis", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Work> getThesisWorks() {
    return workComponent.getThesisWorks();
  }

  @RequestMapping(value="/practice", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Work> getPracticeWorks() {
    return workComponent.getPracticeWorks();
  }

  @RequestMapping(value = "/{workId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public Work getWork(@PathVariable("workId") Long workId) {
    return workComponent.getWorkById(workId);
  }

}
