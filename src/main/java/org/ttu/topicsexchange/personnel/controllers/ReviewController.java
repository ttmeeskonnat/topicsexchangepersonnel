package org.ttu.topicsexchange.personnel.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.ttu.topicsexchange.personnel.model.Review;
import org.ttu.topicsexchange.personnel.model.Work;
import org.ttu.topicsexchange.personnel.services.ReviewComponent;
import org.ttu.topicsexchange.personnel.services.WorkComponent;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.ttu.topicsexchange.personnel.controllers.ControllerSupport.getCurrentPerson;

@Controller
@RequestMapping("/reviews")
public class ReviewController {

  @Autowired
  ReviewComponent reviewComponent;

  @Autowired
  WorkComponent workComponent;

  @RequestMapping(value = "/find/{workId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public List<Review> findReviewsForWork(@PathVariable("workId") Long workId) {
    return reviewComponent.findReviewsForWork(workId);
  }

  @RequestMapping(value = "/{reviewId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public Review getReview(@PathVariable("reviewId") Long reviewId) {
    return reviewComponent.getReview(reviewId);
  }

  @RequestMapping(value = "/add/{workId}", method = RequestMethod.POST)
  @ResponseStatus(value = HttpStatus.OK)
  public void addReview(@PathVariable(value = "workId") Long workId,
                        @RequestParam(value = "title", required = true) String title,
                        @RequestParam(value = "file", required = true) MultipartFile multipartFile) throws IOException {
    Work work = workComponent.getWorkById(workId);
    byte[] data = multipartFile.getBytes();
    Review review = new Review();
    review.setWork(work);
    review.setFileName(multipartFile.getOriginalFilename());
    review.setFileSize(data.length);
    review.setTitle(title);
    review.setReviewer(getCurrentPerson());
    review.setDateOfReview(new Date());
    reviewComponent.addReview(review, data);
  }

  @RequestMapping(value = "/update/{reviewId}", method = RequestMethod.POST)
  @ResponseStatus(value = HttpStatus.OK)
  public void updateReview(@PathVariable("reviewId") Long reviewId,
                           @RequestParam(value = "title", required = true) String title) {
    //todo add checks if one can edit?
    Review review = reviewComponent.getReview(reviewId);
    review.setTitle(title);
//    review.setDateOfReview(new Date());
    reviewComponent.updateReview(review);
  }

  @RequestMapping(value = "/file/{reviewId}", method = RequestMethod.POST)
  @ResponseStatus(value = HttpStatus.OK)
  public void storeReviewFile(@PathVariable("reviewId") Long reviewId,
                              @RequestParam(value = "file", required = true) MultipartFile multipartFile) throws IOException {
    Review review = reviewComponent.getReview(reviewId);
    byte[] data = multipartFile.getBytes();
    review.setFileName(multipartFile.getOriginalFilename());
    review.setFileSize(data.length);
    reviewComponent.storeReviewData(review, data);
  }

  @RequestMapping(value = "/file/{reviewId}", method = RequestMethod.DELETE)
  @ResponseStatus(value = HttpStatus.OK)
  public void removeReviewFile(@PathVariable("reviewId") Long reviewId) {
    Review review = reviewComponent.getReview(reviewId);
    review.setFileName(null);
    review.setFileSize(0);
    reviewComponent.removeReviewData(reviewId);
    reviewComponent.updateReview(review);
  }

  @RequestMapping(value = "/file/{reviewId}", method = RequestMethod.GET)
  @ResponseBody
  public ResponseEntity<byte[]> fetchReviewFile(@PathVariable("reviewId") Long reviewId) {
    Review review = reviewComponent.getReview(reviewId);
    HttpHeaders headers = new HttpHeaders();
    headers.setContentLength(review.getFileSize());
    headers.setContentDispositionFormData("attachment", review.getFileName());
    headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
    return new ResponseEntity<>(reviewComponent.fetchReviewData(reviewId), headers, HttpStatus.OK);
  }

}
