package org.ttu.topicsexchange.personnel.controllers;

import com.google.gson.Gson;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.ttu.topicsexchange.personnel.authentication.User;

@Controller
@RequestMapping
public class DashboardController {

  @RequestMapping(value = "")
  public String index() {
    return "redirect:/dashboard";
  }

  @RequestMapping(value = "/dashboard")
  public ModelAndView dashboard(UsernamePasswordAuthenticationToken principal) {
    ModelAndView modelAndView = new ModelAndView("index");

    if (principal != null) {
      User user = (User)principal.getPrincipal();
      modelAndView.addObject("username", user.getDisplayName());
      modelAndView.addObject("userRoles", new Gson().toJson(user.getRoles()));
      modelAndView.addObject("userId", user.getId());
    }
    return modelAndView;
  }
}
