package org.ttu.topicsexchange.personnel.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.ttu.topicsexchange.personnel.services.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ExceptionHandlingAdvice {

  static Logger LOG = LoggerFactory.getLogger(ExceptionHandlingAdvice.class);

  private static final GsonBuilder gsonBuilder = getGsonBuilder();

  private static GsonBuilder getGsonBuilder() {
    return new GsonBuilder().setDateFormat("dd.MM.yyyy");
  }

  public Gson getGson() {
    return gsonBuilder.create();
  }

  @ExceptionHandler(value = {RuntimeException.class})
  public @ResponseBody
  String dataAccessError(Exception exception) throws Exception {
    LOG.error("Unexpected error", exception);
    return createGeneralErrorJson("technical error. We apologize for the inconvenience");
  }

  @ExceptionHandler(value = {ServiceException.class})
  public @ResponseBody
  String userExceptionHandler(HttpServletRequest req, ServiceException exception) throws Exception {
    return new JsonBuilder().add("errorCode", exception.getErrorCode())
      .add("userError", exception.getErrorMessage())
      .add("url", req.getRequestURL().toString()).build();
  }

  public String createGeneralErrorJson(String message) {
    Map<String,String> params = new HashMap<>();
    params.put("generalError", message);
    return getGson().toJson(params);
  }

  public class JsonBuilder {
    Map<String, String> params = new HashMap<>();

    public JsonBuilder add(String name, String value) {
      params.put(name, value);
      return this;
    }

    public String build() {
      return getGson().toJson(params);
    }
  }
}
