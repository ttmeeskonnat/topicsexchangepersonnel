package org.ttu.topicsexchange.personnel.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.ttu.topicsexchange.personnel.model.faculty.Curriculum;
import org.ttu.topicsexchange.personnel.model.faculty.Faculty;
import org.ttu.topicsexchange.personnel.services.CurriculumComponent;

import java.util.Collection;

@Controller
@RequestMapping("/curriculum")
public class CurriculumController {

  @Autowired
  CurriculumComponent curriculumComponent;

  @RequestMapping(value="/faculties", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Faculty> getFaculties() {
    return curriculumComponent.getFaculties();
  }

  @RequestMapping(value="/{curriculumId}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Curriculum getCurriculum(@PathVariable("curriculumId") Long curriculumId) {
    return curriculumComponent.getCurriculum(curriculumId);
  }

  @RequestMapping(value="/by_faculty/{facultyId}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Curriculum> getCurriculums(@PathVariable("facultyId") Long facultyId) {
    return curriculumComponent.getCurriculums(facultyId);
  }

  @RequestMapping(value="/by_customer/{customerId}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Curriculum> getCurriculumsForCustomer(@PathVariable("customerId") Long customerId) {
    return curriculumComponent.getCurriculumsByCustomer(customerId);
  }
}
