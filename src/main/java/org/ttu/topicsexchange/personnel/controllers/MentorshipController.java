package org.ttu.topicsexchange.personnel.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.ttu.topicsexchange.personnel.model.Mentorship;
import org.ttu.topicsexchange.personnel.model.Proposal;
import org.ttu.topicsexchange.personnel.model.Work;
import org.ttu.topicsexchange.personnel.model.customers.Customer;
import org.ttu.topicsexchange.personnel.model.customers.Person;
import org.ttu.topicsexchange.personnel.model.customers.Teacher;
import org.ttu.topicsexchange.personnel.services.*;

import java.util.Collection;

import static org.ttu.topicsexchange.personnel.controllers.ControllerSupport.getCurrentPerson;

@Controller
@RequestMapping("/mentorship")
public class MentorshipController {

  private static final String MENTORSHIP_ID = "mentorshipId";
  private static final String WORK_ID = "workId";
  private static final String TEACHER_ID = "teacherId";
  private static final String INVITATION_ID = "invitationId";
  private static final String ASSIGNMENT_ID = "assignmentId";
  private static final String PROPOSAL_ID = "proposalId";

  @Autowired
  CustomerComponent customerComponent;

  @Autowired
  MentorshipComponent mentorshipComponent;

  @Autowired
  ProposalComponent proposalComponent;

  @Autowired
  WorkComponent workComponent;

  @RequestMapping(value="/invitations", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Mentorship> getMyInvitations() {
    return mentorshipComponent.getInvitations(getCurrentPerson());
  }

  @RequestMapping(value="/invitation/{invitationId}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Mentorship getInvitation(@PathVariable(INVITATION_ID) Long invitationId) {
    return mentorshipComponent.getInvitation(getCurrentPerson(), invitationId);
  }

  @RequestMapping(value="/assignments", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Collection<Mentorship> getMyAssignments() {
    return mentorshipComponent.getAssignments(getCurrentPerson());
  }

  @RequestMapping(value="/assignment/{assignmentId}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Mentorship getAssignment(@PathVariable(ASSIGNMENT_ID) Long assignmentId) {
    return mentorshipComponent.getAssignment(getCurrentPerson(), assignmentId);
  }

  @RequestMapping(value="/assign/work/{workId}", method = RequestMethod.GET)
  public @ResponseBody void assign(@PathVariable(WORK_ID) Long workId) {
    Work work = workComponent.getWorkById(workId);
    mentorshipComponent.assign(work, getCurrentPersonAsTeacher());
  }

  @RequestMapping(value="/assign/proposal/{proposalId}", method = RequestMethod.GET)
  public @ResponseBody void assignToProposal(@PathVariable(PROPOSAL_ID) Long proposalId) {
    Proposal proposal = proposalComponent.getProposalById(proposalId);
    mentorshipComponent.assign(proposal, getCurrentPersonAsTeacher());
  }

  @RequestMapping(value="/reassign/{mentorshipId}/{teacherId}", method = RequestMethod.GET)
  public @ResponseBody void reassign(@PathVariable(MENTORSHIP_ID) Long mentorshipId, @PathVariable(TEACHER_ID) Long teacherId) {
    Mentorship mentorship = mentorshipComponent.getMentorship(mentorshipId);
    Teacher teacher = customerComponent.findTeacherById(teacherId);
    mentorshipComponent.reassign(mentorship, teacher);
  }

  @RequestMapping(value="/accept/{mentorshipId}", method = RequestMethod.GET)
  public @ResponseBody void accept(@PathVariable(MENTORSHIP_ID) String mentorshipId) {
    Mentorship mentorship = mentorshipComponent.findMentorship(getCurrentPerson(), Long.valueOf(mentorshipId));
    mentorshipComponent.accept(mentorship);
  }

  @RequestMapping(value="/decline/{mentorshipId}", method = RequestMethod.GET)
  public @ResponseBody void decline(@PathVariable(MENTORSHIP_ID) String mentorshipId) {
    Mentorship mentorship = mentorshipComponent.findMentorship(getCurrentPerson(), Long.valueOf(mentorshipId));
    mentorshipComponent.decline(mentorship);
  }

  @RequestMapping(value="/get/{mentorshipId}", method = RequestMethod.GET, produces = "application/json")
  public @ResponseBody Mentorship getMentorship(@PathVariable(MENTORSHIP_ID) Long mentorshipId) {
    return mentorshipComponent.getMentorship(mentorshipId);
  }

  private Teacher getCurrentPersonAsTeacher() {
    Person currentPerson = getCurrentPerson();
    if (!currentPerson.getType().equals(Customer.Type.TEACHER)) {
      throw new ServiceException(ServiceException.Error.CURRENT_USER_IS_NOT_TEACHER);
    }
    return (Teacher)currentPerson;
  }

}
