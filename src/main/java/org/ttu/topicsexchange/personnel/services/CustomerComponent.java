package org.ttu.topicsexchange.personnel.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ttu.topicsexchange.personnel.mocks.TestDataFeeder;
import org.ttu.topicsexchange.personnel.model.customers.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toSet;
import static org.ttu.topicsexchange.personnel.model.customers.Customer.Type.*;

/**
 * todo: This class should refactored after integration with business layer.
 */
@Component
public class CustomerComponent {

  @Autowired
  TestDataFeeder testDataFeeder;

  public Set<Customer> getAllCustomers() {
    return testDataFeeder.getCustomers();
  }

  public Collection<Company> getCompanies() {
    return findByType(COMPANY).stream().map(customer -> (Company) customer).collect(toSet());
  }

  public Collection<Student> getStudents() {
    return findByType(STUDENT).stream().map(customer -> (Student) customer).collect(toSet());
  }

  public Collection<Teacher> getTeachers() {
    return findByType(TEACHER).stream().map(customer -> (Teacher) customer).collect(toSet());
  }

  public Collection<Person> getPersons() {
    Set<Person> persons = new HashSet<>();
    persons.addAll(getTeachers());
    persons.addAll(getStudents());
    return persons;
  }

  public Collection<Company> findCompanyByName(String name) {
    return getCompanies().stream()
      .filter(el -> el.getName().contains(name))
      .collect(toSet());
  }

  public Collection<Student> findStudentsByName(String name) {
    return getStudents().stream()
      .filter(el -> el.getName().contains(name))
      .collect(toSet());
  }

  public Collection<Teacher> findTeachersByName(String name) {
    return getTeachers().stream()
      .filter(el -> el.getName().contains(name))
      .collect(toSet());
  }

  public Teacher findTeacherById(Long id) {
    return getTeachers().stream().filter(el -> el.getId().equals(id)).findFirst().get();
  }

  public Person findPersonById(Long id) {
    return getPersons().stream().filter(el -> el.getId().equals(id)).findFirst().get();
  }

  public Company findCompanyById(Long id) {
    return getCompanies().stream().filter(el -> el.getId().equals(id)).findFirst().get();
  }

  private Collection<Customer> findByType(Customer.Type customerType) {
    return testDataFeeder.getCustomers()
      .stream()
      .filter(customer -> customer.getType().equals(customerType))
      .collect(toSet());
  }
}
