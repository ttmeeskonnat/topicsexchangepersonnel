package org.ttu.topicsexchange.personnel.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ttu.topicsexchange.personnel.mocks.TestDataFeeder;
import org.ttu.topicsexchange.personnel.model.Work;
import org.ttu.topicsexchange.personnel.model.WorkDescriptor;

import java.util.Collection;
import java.util.Optional;

import static java.util.stream.Collectors.toSet;
import static org.ttu.topicsexchange.personnel.services.ServiceException.Error.PROPOSAL_DOES_NOT_EXIST;
import static org.ttu.topicsexchange.personnel.services.ServiceException.Error.WORK_DOES_NOT_EXIST;

@Component
public class WorkComponent {

  @Autowired
  TestDataFeeder testDataFeeder;

  public Collection<Work> getAllWorks() {
    return testDataFeeder.getWorks();
  }

  public Work getWorkById(Long workId) {
    Work work = findWorkById(workId);
    if (work == null) {
      throw new ServiceException(WORK_DOES_NOT_EXIST, "Cannot find work for id: " + workId);
    }
    return work;
  }

  public Work findWorkById(Long workId) {
    Optional<Work> work = getAllWorks()
      .stream()
      .filter(el -> el.getId().equals(workId))
      .findFirst();
    return work.isPresent() ? work.get() : null;
  }

  public Collection<Work> getThesisWorks() {
    return getWorks(WorkDescriptor.Type.THESIS, null);
  }

  public Collection<Work> getPracticeWorks() {
    return getWorks(WorkDescriptor.Type.PRACTICE, null);
  }

  public Collection<Work> getWorks(WorkDescriptor.Type type, Work.Status status) {
    return getAllWorks()
      .stream()
      .filter(status != null ? el -> el.getStatus().equals(status) : el -> el.getStatus() != null)
      .filter(el -> el.getType().equals(type))
      .collect(toSet());
  }
}
