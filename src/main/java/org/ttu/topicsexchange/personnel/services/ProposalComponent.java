package org.ttu.topicsexchange.personnel.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ttu.topicsexchange.personnel.integration.ServiceInvoker;
import org.ttu.topicsexchange.personnel.mocks.TestProposalsFeeder;
import org.ttu.topicsexchange.personnel.model.Proposal;

import java.util.*;

import static org.ttu.topicsexchange.personnel.services.ServiceException.Error.PROPOSAL_DOES_NOT_EXIST;

@Component
public class ProposalComponent {

  @Autowired
  ServiceInvoker serviceInvoker;

  @Autowired
  TestProposalsFeeder testProposalsFeeder;

  public Proposal getProposalById(Long proposalId) {
    Map<String,Object> params = new HashMap<>();
    params.put("proposalId", proposalId);
    Proposal proposal = serviceInvoker.get("/proposals/get/{proposalId}", params, Proposal.class);

    if (proposal == null) {
      throw new ServiceException(PROPOSAL_DOES_NOT_EXIST, "Cannot find proposal for id: " + proposalId);
    }
    return proposal;
  }

  public Collection<Proposal> getActiveProposals() {
    Proposal[] proposals = serviceInvoker.get("/proposals/all", Collections.emptyMap(), Proposal[].class);
    return Arrays.asList(proposals);
  }

  public void add(Proposal proposal) {
    testProposalsFeeder.addProposal(proposal);
  }

  public void update(Proposal proposal) {
    testProposalsFeeder.update(proposal);
  }

  public void archive(Proposal proposal) {
    proposal.setStatus(Proposal.Status.ARCHIVED);
    testProposalsFeeder.update(proposal);
  }
}
