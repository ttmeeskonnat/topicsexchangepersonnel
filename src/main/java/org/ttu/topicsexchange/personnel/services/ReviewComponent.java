package org.ttu.topicsexchange.personnel.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.ttu.topicsexchange.personnel.mocks.TestReviewsFeeder;
import org.ttu.topicsexchange.personnel.model.Review;

import java.util.List;
import java.util.Optional;

import static org.ttu.topicsexchange.personnel.services.ServiceException.Error.REVIEW_DOES_NOT_EXIST;
import static org.ttu.topicsexchange.personnel.services.ServiceException.Error.REVIEW_FILE_DOES_NOT_EXIST;

@Component
public class ReviewComponent {

  @Autowired
  TestReviewsFeeder testReviewsFeeder;

  public List<Review> findReviewsForWork(Long workId) {
    return testReviewsFeeder.findReviewsForWork(workId);
  }

  public Review getReview(Long reviewId) {
//    Review review = new Review();
//    review.setId(reviewId);
//    review.setFileName(reviewId + ".txt");
//    review.setFileSize(fetchReviewData(reviewId).length);
//    return review;
    return Optional.of(testReviewsFeeder.getReview(reviewId))
        .orElseThrow(() -> new ServiceException(REVIEW_DOES_NOT_EXIST, "Cannot find review for id: " + reviewId));
  }

  public void addReview(Review review, byte[] data) {
    Assert.notNull(review.getWork(), "Review work cannot be null");
    Assert.notNull(review.getReviewer(), "Reviewer cannot be null");
    Assert.hasText(review.getFileName(), "Review file name cannot be empty");
    Assert.notNull(data, "Review data cannot be null");
    testReviewsFeeder.addReview(review, data);
  }

  public void updateReview(Review review) {
    testReviewsFeeder.updateReview(review);
  }

  public void storeReviewData(Review review, byte[] data) {
    Assert.notNull(review.getId(), "Review ID cannot be null");
    Assert.hasText(review.getFileName(), "Review file name cannot be empty");
    Assert.notNull(data, "Review data cannot be null");
    testReviewsFeeder.storeReviewData(review.getId(), data);
    updateReview(review);
  }

  public void removeReviewData(Long reviewId) {
    Assert.notNull(reviewId, "Review ID cannot be null");
    testReviewsFeeder.removeReviewData(reviewId);
  }

  public byte[] fetchReviewData(Long reviewId) {
//    return ("Review " + reviewId).getBytes();
    return Optional.of(testReviewsFeeder.fetchReviewData(reviewId))
        .orElseThrow(() -> new ServiceException(REVIEW_FILE_DOES_NOT_EXIST, "Cannot find review file for id: " + reviewId));
  }

}
