package org.ttu.topicsexchange.personnel.services;


public class ServiceException extends RuntimeException {

  public enum Error {
    USER_DOES_NOT_EXIST("1", "Requested user does not exist"),
    PROPOSAL_DOES_NOT_EXIST("2", "Requested proposal does not exist"),
    WORK_DOES_NOT_EXIST("3", "Requested work does not exist"),
    MENTORSHIP_DOES_NOT_EXIST("4", "Requested mentorship does not exist"),
    CURRENT_USER_IS_NOT_TEACHER("5", "Current user is not a teacher"),
    FACULTY_DOES_NOT_EXIST("6", "Faculty does not exist"),
    PROPOSAL_UPDATING_DENIED("7", "The current person is not allowed to update the proposal"),
    PERSON_DOES_NOT_EXIST("8", "Person does not exist"),
    CURRICULUM_DOES_NOT_EXIST("9", "Curriculum does not exist"),
    REVIEW_DOES_NOT_EXIST("10", "Review does not exist"),
    REVIEW_FILE_DOES_NOT_EXIST("11", "Review file does not exist"),
    TEACHER_IS_ALREADY_MENTOR_FOR_GIVEN_WORK("12", "The teacher is already mentor for given work");

    String errorCode;
    String errorMessage;

    Error(String errorCode, String errorMessage) {
      this.errorCode = errorCode;
      this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
      return errorCode;
    }

    public String getErrorMessage() {
      return errorMessage;
    }
  }

  Error error;
  String errorDetails;

  public ServiceException(Error error) {
    this(error, null);
  }

  public ServiceException(Error error, String errorDetails) {
    this.error = error;
    this.errorDetails = errorDetails;
  }

  public Error getError() {
    return error;
  }

  public String getErrorCode() {
    return error.errorCode;
  }

  public String getErrorMessage() {
    return error.errorMessage;
  }

  public String getErrorDetails() {
    return errorDetails;
  }
}

