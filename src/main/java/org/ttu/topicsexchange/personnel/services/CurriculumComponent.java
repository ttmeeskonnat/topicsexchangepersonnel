package org.ttu.topicsexchange.personnel.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ttu.topicsexchange.personnel.mocks.TestCustomersFeeder;
import org.ttu.topicsexchange.personnel.mocks.TestFacultyFeeder;
import org.ttu.topicsexchange.personnel.model.customers.Person;
import org.ttu.topicsexchange.personnel.model.faculty.Curriculum;
import org.ttu.topicsexchange.personnel.model.faculty.Faculty;

import java.util.Collection;
import java.util.Set;

import static org.ttu.topicsexchange.personnel.services.ServiceException.Error.CURRICULUM_DOES_NOT_EXIST;
import static org.ttu.topicsexchange.personnel.services.ServiceException.Error.FACULTY_DOES_NOT_EXIST;

@Component
public class CurriculumComponent {

  @Autowired
  TestFacultyFeeder facultyFeeder;
  @Autowired
  TestCustomersFeeder customersFeeder;

  public Set<Faculty> getFaculties() {
    return facultyFeeder.getFaculties();
  }

  public Collection<Curriculum> getCurriculums(Long facultyId) {
    Faculty faculty = facultyFeeder.getFacultyById(facultyId);
    if (faculty == null) {
      throw new ServiceException(FACULTY_DOES_NOT_EXIST, "Cannot find faculty for id: " + facultyId);
    }
    return facultyFeeder.getCurriculums(faculty);
  }

  public Collection<Curriculum> getCurriculumsByCustomer(Long personId) {
    Person person = customersFeeder.getPerson(personId);
    if (person == null) {
      throw new ServiceException(FACULTY_DOES_NOT_EXIST, "Cannot find person for id: " + personId);
    }

    return facultyFeeder.getCurriculums(person);
  }

  public Curriculum getCurriculum(Long curriculumId) {
    Curriculum curriculum = facultyFeeder.getCurriculum(curriculumId);
    if (curriculum == null) {
      throw new ServiceException(CURRICULUM_DOES_NOT_EXIST, "Cannot find curriculum for id: " + curriculumId);
    }
    return curriculum;
  }
}
