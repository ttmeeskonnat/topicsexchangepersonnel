package org.ttu.topicsexchange.personnel.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ttu.topicsexchange.personnel.mocks.TestWorksFeeder;
import org.ttu.topicsexchange.personnel.model.Mentorship;
import org.ttu.topicsexchange.personnel.model.WorkDescriptor;
import org.ttu.topicsexchange.personnel.model.customers.Person;
import org.ttu.topicsexchange.personnel.model.customers.Teacher;

import java.util.Collection;
import java.util.Optional;

import static java.util.stream.Collectors.toSet;
import static org.ttu.topicsexchange.personnel.services.ServiceException.Error.MENTORSHIP_DOES_NOT_EXIST;
import static org.ttu.topicsexchange.personnel.services.ServiceException.Error.TEACHER_IS_ALREADY_MENTOR_FOR_GIVEN_WORK;

@Component
public class MentorshipComponent {

  @Autowired
  TestWorksFeeder testWorksFeeder;

  public Mentorship getMentorship(Long mentorshipId) {
    return testWorksFeeder.getMentorships()
      .stream()
      .filter(el -> el.getId().equals(mentorshipId))
      .findAny().get();
  }

  public Mentorship getMentorship(Person person, Long mentorshipId, Mentorship.Type type) {
    Optional<Mentorship> mentorship = testWorksFeeder.getMentorships()
      .stream()
      .filter(el -> el.getMentor().equals(person) && el.getId().equals(mentorshipId))
      .filter(el -> el.getType().equals(type))
      .findAny();

    if (!mentorship.isPresent()) {
      throw new ServiceException(MENTORSHIP_DOES_NOT_EXIST,
        "Cannot find mentorship for id: " + mentorshipId + " and person: " + person.getId());
    }

    return mentorship.get();
  }

  public Collection<Mentorship> getInvitations(Person person) {
    return testWorksFeeder.getMentorships()
      .stream()
      .filter(el -> el.getMentor().equals(person))
      .filter(el -> el.getType().equals(Mentorship.Type.INVITATION))
      .collect(toSet());
  }

  public Mentorship getInvitation(Person person, Long invitationId) {
    return getMentorship(person, invitationId, Mentorship.Type.INVITATION);
  }

  public Mentorship getAssignment(Person person, Long assignmentId) {
    return getMentorship(person, assignmentId, Mentorship.Type.ASSIGNMENT);
  }

  public Collection<Mentorship> getAssignments(Person person) {
    return testWorksFeeder.getMentorships()
      .stream()
      .filter(el -> el.getMentor().equals(person))
      .filter(el -> el.getType().equals(Mentorship.Type.ASSIGNMENT))
      .collect(toSet());
  }

  public Mentorship findMentorship(Person person, Long mentorshipId) {
    Optional<Mentorship> mentorship = testWorksFeeder.getMentorships()
      .stream()
      .filter(el -> el.getMentor().equals(person))
      .filter(el -> el.getId().equals(mentorshipId))
      .findAny();

    if (!mentorship.isPresent()) {
      throw new ServiceException(MENTORSHIP_DOES_NOT_EXIST,
        "Cannot find mentorship for id: " + mentorshipId + " and person: " + person.getId());
    }

    return mentorship.get();
  }

  public void accept(Mentorship mentorship) {
    mentorship.setType(Mentorship.Type.ASSIGNMENT);
  }

  public void reassign(Mentorship mentorship, Teacher teacher) {
    // todo: check that selected teacher has less than allowed number of assignments
    mentorship.setMentor(teacher);
  }

  public void decline(Mentorship mentorship) {
    testWorksFeeder.removeMentorship(mentorship);
  }

  public void assign(WorkDescriptor work, Teacher teacher) {
    Mentorship mentorship = new Mentorship();
    mentorship.setType(Mentorship.Type.ASSIGNMENT);
    mentorship.setMentor(teacher);
    mentorship.setWork(work);
    if (testWorksFeeder.hasMentorship(mentorship)) {
      throw new ServiceException(TEACHER_IS_ALREADY_MENTOR_FOR_GIVEN_WORK,
        "Teacher with id=" + teacher.getId() + " has mentorship for work with id=" + work.getId());
    }
    testWorksFeeder.addMentorship(mentorship);
  }
}
