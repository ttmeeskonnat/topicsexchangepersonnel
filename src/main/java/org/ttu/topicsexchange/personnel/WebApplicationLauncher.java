package org.ttu.topicsexchange.personnel;

import org.eclipse.jetty.annotations.AnnotationConfiguration;
import org.eclipse.jetty.annotations.ClassInheritanceHandler;
import org.eclipse.jetty.plus.webapp.EnvConfiguration;
import org.eclipse.jetty.plus.webapp.PlusConfiguration;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.ConcurrentHashSet;
import org.eclipse.jetty.webapp.*;
import org.springframework.web.WebApplicationInitializer;
import org.ttu.topicsexchange.personnel.config.SpringMvcInitializer;
import org.ttu.topicsexchange.personnel.config.SpringSecurityInitializer;

import java.io.File;
import java.net.URI;

public class WebApplicationLauncher {

  public static void main(String[] args) throws Exception {
    Server server = new Server(8080);

    // TODO: replace with your local path to webapp folder
    URI webappPath = resolveWebappPath();

    WebAppContext webAppContext = new WebAppContext("topicsexchange", "/*");
    webAppContext.setContextPath("/*");
    webAppContext.setResourceBase(webappPath.getPath());
    webAppContext.setClassLoader(Thread.currentThread().getContextClassLoader());

    webAppContext.setConfigurations(new Configuration[]{
      new WebInfConfiguration(),
      new FragmentConfiguration(),
      new EnvConfiguration(),
      new PlusConfiguration(),
      new MetaInfConfiguration(),
      new AnnotationConfiguration() {
        @Override
        public void preConfigure(WebAppContext context) throws Exception {
          ClassInheritanceMap classInheritanceMap = new ClassInheritanceMap();
          ConcurrentHashSet<String> set = new ConcurrentHashSet<>();
          set.add(SpringMvcInitializer.class.getName());
          set.add(SpringSecurityInitializer.class.getName());
          classInheritanceMap.put(WebApplicationInitializer.class.getName(), set);
          context.setAttribute(CLASS_INHERITANCE_MAP, classInheritanceMap);
          _classInheritanceHandler = new ClassInheritanceHandler(classInheritanceMap);
        }
      }
    });

    server.setHandler(webAppContext);
    server.start();
    server.join();
  }

  private static URI resolveWebappPath() {
    URI webappPath = new File(System.getProperty("webapp.location", "/home/vlad/projects/topicsexchange/src/main/webapp")).toURI();
    return new File(webappPath).toURI();
//
//    String path = WebApplicationLauncher.class.getClassLoader().getResource(".").getPath();
//    int classesMainIdx = path.indexOf("/classes/main");
//    return new File(path.substring(0, classesMainIdx) + "/inplaceWebapp").toURI();
  }
}

