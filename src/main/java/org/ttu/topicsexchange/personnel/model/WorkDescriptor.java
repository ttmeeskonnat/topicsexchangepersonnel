package org.ttu.topicsexchange.personnel.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.ttu.topicsexchange.personnel.model.customers.Customer;
import org.ttu.topicsexchange.personnel.model.customers.Teacher;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

// todo: may be someone can propose better name for this class
abstract public class WorkDescriptor implements Serializable {

  public enum Type {THESIS, PRACTICE}

  private Long id;
  private String title;
  private String description;
  private Type type;
  @JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="class")
  private Customer author;
  private Set<Teacher> mentors = new HashSet<>();
  private Set<Comment> comments = new HashSet<>();;
  private Date creationDate;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Set<Comment> getComments(){
    return comments;
  }

  public void setComments(Set<Comment> comments){
    this.comments = comments;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public Customer getAuthor() {
    return author;
  }

  public void setAuthor(Customer author) {
    this.author = author;
  }

  public Set<Teacher> getMentors() {
    return mentors;
  }

  public void addMentor(Teacher teacher) {
    mentors.add(teacher);
  }

  public void removeMentor(Teacher teacher) {
    mentors.remove(teacher);
  }

  public boolean isMentor(Teacher teacher) {
    return mentors.contains(teacher);
  }

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Proposal)) return false;

    WorkDescriptor descriptor = (WorkDescriptor) o;
    return !(id != null ? !id.equals(descriptor.id) : descriptor.id != null);
  }

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }
}