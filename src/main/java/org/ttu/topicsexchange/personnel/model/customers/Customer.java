package org.ttu.topicsexchange.personnel.model.customers;

import java.io.Serializable;

public abstract class Customer implements Serializable {

  public enum Type {TEACHER, STUDENT, COMPANY}

  private Long id;
  private String email;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public abstract Type getType();
  public abstract String getName();

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Customer)) return false;
    Customer customer = (Customer) o;
    return !(id != null ? !id.equals(customer.id) : customer.id != null);
  }

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }
}
