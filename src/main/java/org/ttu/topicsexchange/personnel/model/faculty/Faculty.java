package org.ttu.topicsexchange.personnel.model.faculty;


public class Faculty {

  Long id;
  String name;

  public Faculty(Long id, String name) {
    this.id = id;
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Faculty)) return false;

    Faculty faculty = (Faculty) o;
    return name.equals(faculty.name);
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }
}
