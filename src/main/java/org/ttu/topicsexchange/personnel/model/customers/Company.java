package org.ttu.topicsexchange.personnel.model.customers;


public class Company extends Customer {

  String companyName;

  public void setName(String companyName) {
    this.companyName = companyName;
  }

  @Override
  public String getName() {
    return companyName;
  }

  @Override
  public Type getType() {
    return Type.COMPANY;
  }
}
