package org.ttu.topicsexchange.personnel.model;

import org.ttu.topicsexchange.personnel.model.customers.Student;

import java.util.Set;

public class Proposal extends WorkDescriptor {

  public enum Status {ACTIVE,ARCHIVED}

  private Set<Student> wishers;
  private Status status;

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public Set<Student> getWishers() {
    return wishers;
  }

  public void setWishers(Set<Student> wishers) {
    this.wishers = wishers;
  }
}
