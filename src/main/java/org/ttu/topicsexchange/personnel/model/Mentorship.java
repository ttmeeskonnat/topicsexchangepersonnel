package org.ttu.topicsexchange.personnel.model;

import org.ttu.topicsexchange.personnel.model.customers.Student;
import org.ttu.topicsexchange.personnel.model.customers.Teacher;

import java.io.Serializable;

public class Mentorship implements Serializable {

  public enum Type {INVITATION, ASSIGNMENT}

  Long id;
  Type type;
  WorkDescriptor work; // it can be proposal or actual work
  Student student;
  Teacher mentor;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public WorkDescriptor getWork() {
    return work;
  }

  public void setWork(WorkDescriptor work) {
    this.work = work;
  }

  public Student getStudent() {
    return student;
  }

  public void setStudent(Student student) {
    this.student = student;
  }

  public Teacher getMentor() {
    return mentor;
  }

  public void setMentor(Teacher mentor) {
    this.mentor = mentor;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Mentorship)) return false;

    Mentorship that = (Mentorship) o;
    return !(id != null ? !id.equals(that.id) : that.id != null);
  }

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }
}
