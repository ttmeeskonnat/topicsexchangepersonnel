package org.ttu.topicsexchange.personnel.model;

import org.ttu.topicsexchange.personnel.model.customers.Person;
import org.ttu.topicsexchange.personnel.model.customers.Student;

import java.util.HashSet;
import java.util.Set;

public class Work extends WorkDescriptor {
  public enum Status {IN_PROGRESS, FINISHED, ARCHIVED}

  private Status status;
  private Student assignee;
  private Set<Person> reviewers = new HashSet<>();

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public Student getAssignee() {
    return assignee;
  }

  public void setAssignee(Student assignee) {
    this.assignee = assignee;
  }

  public Set<Person> getReviewers() {
    return reviewers;
  }

  public void setReviewers(Set<Person> reviewers) {
    this.reviewers = reviewers;
  }
}
