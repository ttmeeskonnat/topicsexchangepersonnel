package org.ttu.topicsexchange.personnel.model.customers;

public class Teacher extends Person {

  @Override
  public Type getType() {
    return Type.TEACHER;
  }
}
