package org.ttu.topicsexchange.personnel.model;

import org.ttu.topicsexchange.personnel.model.customers.Customer;

public class Comment {
  private Long id;
  private String text;
  private Customer author;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getText(){
    return this.text;
  }

  public Customer getAuthor() {
    return author;
  }

  public void setAuthor(Customer author) {
    this.author = author;
  }
}
