package org.ttu.topicsexchange.personnel.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.ttu.topicsexchange.personnel.model.customers.Person;

import java.util.Date;

public class Review {

  Long id;
  String title;
  Work work;
  Person reviewer;
  Date dateOfReview;
  String fileName;
  long fileSize;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Work getWork() {
    return work;
  }

  public void setWork(Work work) {
    this.work = work;
  }

  public Person getReviewer() {
    return reviewer;
  }

  public void setReviewer(Person reviewer) {
    this.reviewer = reviewer;
  }

  public Date getDateOfReview() {
    return dateOfReview;
  }

  public void setDateOfReview(Date dateOfReview) {
    this.dateOfReview = dateOfReview;
  }

  @JsonProperty
  public String getFileName() {
    return fileName;
  }

  @JsonIgnore
  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  @JsonProperty
  public long getFileSize() {
    return fileSize;
  }

  @JsonIgnore
  public void setFileSize(long fileSize) {
    this.fileSize = fileSize;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Review)) return false;

    Review review = (Review) o;
    return !(id != null ? !id.equals(review.id) : review.id != null);
  }

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }
}
