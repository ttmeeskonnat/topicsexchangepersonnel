package org.ttu.topicsexchange.personnel.model.faculty;

public class Course {

  private Long id;
  private String code;
  private String name;
  private String description;
  private Double ectsCredits;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setEctsCredits(Double ectsCredits) {
    this.ectsCredits = ectsCredits;
  }

  public Double getEctsCredits() {
    return ectsCredits;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Course)) return false;

    Course course = (Course) o;
    return id.equals(course.id);
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }
}
