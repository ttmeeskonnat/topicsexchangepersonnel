package org.ttu.topicsexchange.personnel.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.ttu.topicsexchange.personnel.authentication.Role;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  @Qualifier("authenticationProvider")
  AuthenticationProvider authenticationProvider;

  @Autowired
  public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(authenticationProvider);
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
      .antMatchers("/dashboard**").access(hasAnyRole(Role.CURRICULUM_MANAGER, Role.TEACHER))
      .antMatchers("/customers/**").access(hasAnyRole(Role.CURRICULUM_MANAGER, Role.TEACHER))
      .antMatchers("/proposals/**").access(hasAnyRole(Role.CURRICULUM_MANAGER, Role.TEACHER))
      .antMatchers("/curriculum/**").access(hasAnyRole(Role.CURRICULUM_MANAGER))
      .and().formLogin()
      .loginPage("/login").failureUrl("/login?error")
      .usernameParameter("username")
      .passwordParameter("password")
      .and().logout().logoutSuccessUrl("/login?logout").and().csrf().disable();
  }

  private String hasAnyRole(Role... roles) {
    if (roles.length > 0) {
      StringBuilder sb = new StringBuilder(100);
      sb.append("hasAnyRole(");
      for (int i = 0; i < roles.length; i++) {
        sb.append(i > 0 ? ",'" : "'").append(roles[i]).append("'");
      }
      sb.append(")");
      return sb.toString();
    }
    return "";
  }
}