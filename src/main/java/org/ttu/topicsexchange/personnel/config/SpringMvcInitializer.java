package org.ttu.topicsexchange.personnel.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class[] { ApplicationConfiguration.class };
  }

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class<?>[0];
  }

//  @Override
//  protected Filter[] getServletFilters() {
//    return new Filter[] {new MultipartFilter()};
//  }

//  @Override
//  protected void customizeRegistration(ServletRegistration.Dynamic registration) {
//    super.customizeRegistration(registration);
//    File uploadDirectory = new File(System.getProperty("java.io.tmpdir"));
//    MultipartConfigElement multipartConfigElement = new MultipartConfigElement(uploadDirectory.getAbsolutePath());
//    registration.setMultipartConfig(multipartConfigElement);
//  }

  @Override
  protected String[] getServletMappings() {
    return new String[] { "/" };
  }
}
