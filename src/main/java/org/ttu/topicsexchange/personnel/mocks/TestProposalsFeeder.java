package org.ttu.topicsexchange.personnel.mocks;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ttu.topicsexchange.personnel.model.Proposal;
import org.ttu.topicsexchange.personnel.model.WorkDescriptor;
import org.ttu.topicsexchange.personnel.model.customers.Customer;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.ttu.topicsexchange.personnel.mocks.TestDataFeeder.nextId;
import static org.ttu.topicsexchange.personnel.model.Proposal.Status.ACTIVE;
import static org.ttu.topicsexchange.personnel.model.WorkDescriptor.Type.PRACTICE;
import static org.ttu.topicsexchange.personnel.model.WorkDescriptor.Type.THESIS;

@Component
public class TestProposalsFeeder implements InitializingBean {

  @Autowired
  TestCustomersFeeder customersFeeder;

  private Set<Proposal> TEST_PROPOSALS;

  @Override
  public void afterPropertiesSet() throws Exception {
    TEST_PROPOSALS = prepareProposals();
  }

  public Set<Proposal> getProposals() {
    return TEST_PROPOSALS;
  }

  public void addProposal(Proposal proposal) {
    proposal.setId(nextId());
    proposal.setStatus(ACTIVE);
    TEST_PROPOSALS.add(proposal);
  }

  public void update(Proposal proposal) {
    TEST_PROPOSALS.add(proposal);
  }

  private Set<Proposal> prepareProposals() {
    Set<Proposal> proposals = new HashSet<>();
    proposals.add(proposal("proposal_1", "description", THESIS, customersFeeder.getRandomCompany()));
    proposals.add(proposal("practice 1", "job description", PRACTICE, customersFeeder.getRandomCompany()));
    proposals.add(proposal("practice 2", "job description", PRACTICE, customersFeeder.getRandomCompany()));
    return proposals;
  }

  private Proposal proposal(String title, String description, WorkDescriptor.Type type, Customer author) {
    Proposal proposal = new Proposal();
    proposal.setId((nextId()));
    proposal.setTitle(title);
    proposal.setDescription(description);
    proposal.setType(type);
    proposal.setStatus(ACTIVE);
    proposal.setAuthor(author);
    proposal.setCreationDate(new Date());
    return proposal;
  }
}
