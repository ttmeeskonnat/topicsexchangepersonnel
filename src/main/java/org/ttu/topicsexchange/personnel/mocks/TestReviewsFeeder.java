package org.ttu.topicsexchange.personnel.mocks;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.springframework.stereotype.Component;
import org.ttu.topicsexchange.personnel.model.Review;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.ttu.topicsexchange.personnel.mocks.TestDataFeeder.nextId;

@Component
public class TestReviewsFeeder {

  private Map<Long, byte[]> reviewDataMap = new HashMap<>();

  private Map<Long, Review> reviewMap = new HashMap<>();

  private Multimap<Long, Long> workReviewMap = HashMultimap.create();

  public byte[] fetchReviewData(Long reviewId) {
    return reviewDataMap.get(reviewId);
  }

  public void storeReviewData(Long reviewId, byte[] data) {
    reviewDataMap.put(reviewId, data);
  }

  public void removeReviewData(Long reviewId) {
    reviewDataMap.remove(reviewId);
  }

  public List<Review> findReviewsForWork(Long workId) {
    return workReviewMap.get(workId).stream()
        .map(reviewMap::get)
        .filter(r -> r != null)
        .collect(Collectors.toList());
  }

  public Review getReview(Long reviewId) {
    return reviewMap.get(reviewId);
  }

  public void addReview(Review review, byte[] data) {
    if (review.getId() == null) {
      review.setId(nextId());
    }
    Long reviewId = review.getId();
    workReviewMap.put(reviewId, review.getWork().getId());
    reviewMap.put(reviewId, review);
    storeReviewData(reviewId, data);
  }

  public void updateReview(Review review) {
    reviewMap.put(review.getId(), review);
  }

}
