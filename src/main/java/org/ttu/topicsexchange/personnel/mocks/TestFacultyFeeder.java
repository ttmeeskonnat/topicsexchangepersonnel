package org.ttu.topicsexchange.personnel.mocks;

import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ttu.topicsexchange.personnel.model.customers.Customer;
import org.ttu.topicsexchange.personnel.model.customers.Person;
import org.ttu.topicsexchange.personnel.model.customers.Student;
import org.ttu.topicsexchange.personnel.model.customers.Teacher;
import org.ttu.topicsexchange.personnel.model.faculty.Course;
import org.ttu.topicsexchange.personnel.model.faculty.Curriculum;
import org.ttu.topicsexchange.personnel.model.faculty.Faculty;

import java.util.*;

import static org.ttu.topicsexchange.personnel.mocks.TestCustomersFeeder.NUMBER_OF_STUDENTS;
import static org.ttu.topicsexchange.personnel.mocks.TestDataFeeder.nextId;

@Component
public class TestFacultyFeeder implements InitializingBean {

  private static final String FACULTY_OF_INFORMATION_TECHNOLOGY = "Faculty of Information Technology";
  private static final String CURRICULA_INFORMATICS = "Informatics";

  @Autowired
  TestCustomersFeeder customersFeeder;

  private Set<Faculty> TEST_FACULTIES;
  private Set<Curriculum> TEST_CURRICULUMS;
  private Multimap<Faculty, Curriculum> FACULTIES_TO_CURRICULUM_MAPPING;
  private Multimap<Person, Curriculum> PERSON_TO_CURRICULUM_MAPPING;

  @Override
  public void afterPropertiesSet() throws Exception {
    TEST_FACULTIES = prepareFaculties();
    TEST_CURRICULUMS = prepareCurriculum();
    FACULTIES_TO_CURRICULUM_MAPPING = mapFacultiesToCurriculums();
    PERSON_TO_CURRICULUM_MAPPING = mapPersonsToCurriculums();
  }

  public Set<Faculty> getFaculties() {
    return TEST_FACULTIES;
  }

  public Faculty getFacultyById(Long facultyId) {
    Optional<Faculty> faculty = TEST_FACULTIES.stream().filter(el -> el.getId().equals(facultyId)).findAny();
    return faculty.isPresent() ? faculty.get() : null;
  }

  public Faculty getFacultyByName(String name) {
    Optional<Faculty> faculty = TEST_FACULTIES.stream().filter(el -> el.getName().equals(name)).findAny();
    return faculty.isPresent() ? faculty.get() : null;
  }

  public Curriculum getCurriculum(Long curriculumId) {
    Optional<Curriculum> curriculum = TEST_CURRICULUMS.stream().filter(el -> el.getId().equals(curriculumId)).findAny();
    return curriculum.isPresent() ? curriculum.get() : null;
  }

  public Collection<Curriculum> getCurriculums() {
    return TEST_CURRICULUMS;
  }

  public Collection<Curriculum> getCurriculums(Faculty faculty) {
    return FACULTIES_TO_CURRICULUM_MAPPING.get(faculty);
  }

  public Collection<Curriculum> getCurriculums(Person person) {
    return PERSON_TO_CURRICULUM_MAPPING.get(person);
  }

  public Curriculum getCurriculumByName(String name) {
    Optional<Curriculum> curriculum = TEST_CURRICULUMS.stream().filter(el -> el.getName().equals(name)).findAny();
    return curriculum.isPresent() ? curriculum.get() : null;
  }

  private Set<Faculty> prepareFaculties() {
    Set<Faculty> faculties = new LinkedHashSet<>();
    faculties.add(faculty(FACULTY_OF_INFORMATION_TECHNOLOGY));
    faculties.add(faculty("Faculty of Mechanical Engineering"));
    faculties.add(faculty("Faculty of Power Engineering"));
    faculties.add(faculty("Faculty of Social Sciences"));
    faculties.add(faculty("Faculty of Chemical and Materials Technology"));
    return faculties;
  }

  private Set<Curriculum> prepareCurriculum() {
    Set<Curriculum> curriculums = new LinkedHashSet<>();
    curriculums.add(prepareInformationTechnologyCurriculum());
    return curriculums;
  }

  private Multimap<Faculty, Curriculum> mapFacultiesToCurriculums() {
    Multimap<Faculty, Curriculum> mapping = LinkedHashMultimap.create();
    mapping.put(getFacultyByName(FACULTY_OF_INFORMATION_TECHNOLOGY), getCurriculumByName(CURRICULA_INFORMATICS));
    return mapping;
  }

  private Multimap<Person, Curriculum> mapPersonsToCurriculums() {
    Multimap<Person, Curriculum> mapping = LinkedHashMultimap.create();
    Collection<Curriculum> curriculums = getCurriculums();
    int c = 0;
    Iterator<Customer> studentsIterator = customersFeeder.getStudents().iterator();
    int studentPerCurriculum = NUMBER_OF_STUDENTS / curriculums.size();
    for (Curriculum curriculum : curriculums) {
      // assign curriculum for all teachers
      for (Customer customer : customersFeeder.getTeachers()) {
        mapping.put((Teacher)customer, curriculum);
      }

//      for (int i = 0; i < NUMBER_OF_TEACHERS / 2; i++) { //
//        mapping.put(customersFeeder.getRandomTeacher(), curriculum);
//      }

      for (int s = c * studentPerCurriculum; s < (c+1) * studentPerCurriculum; c++) {
        if (studentsIterator.hasNext()) {
          mapping.put((Student) studentsIterator.next(), curriculum);
        }
      }
      c++;
    }

    return mapping;
  }

  private Curriculum prepareInformationTechnologyCurriculum() {
    Curriculum curriculum = new Curriculum();
    curriculum.setId(nextId());
    curriculum.setName(CURRICULA_INFORMATICS);
    curriculum.setCourses(prepareInformaticsCourses());
    return curriculum;
  }

  private Set<Course> prepareInformaticsCourses() {
    Set<Course> courses = new LinkedHashSet<>();
    courses.add(course("HLI3001", "Giving Presentations in English", 3.0));
    courses.add(course("HLX8040", "Foreign Language for Science and Research,", 3.0));
    courses.add(course("ITV0071", "Semantics and Analytical Philosophy", 5.0));
    courses.add(course("TMJ3331", "Entrepreneurship and Business Planning", 4.0));
    courses.add(course("ITT0050", "Recursion and Complexity Theory", 5.0));
    courses.add(course("YMR0050", "Operations Research", 6.0));
    courses.add(course("IDU1550", "Software architecture and design", 6.0));
    courses.add(course("IDX1511", "Software Processes and Quality", 6.0));
    courses.add(course("ITI8600", "Methods of Knowledge Based Software Development", 6.0));
    courses.add(course("ITI8610", "Software Assurance", 6.0));
    courses.add(course("ITX8301", "Master seminar I", 3.0));
    courses.add(course("ITX8302", "Master seminar II", 3.0));
    courses.add(course("IDU0080", "Web Services and the Architecture of Web Solutions", 5.0));
    courses.add(course("IDU0191", "Process Engineering in Information Systems Development", 5.0));
    courses.add(course("IDU0192", "Process Engineering in Information Systems Development - project", 2.0));
    courses.add(course("IDU0210", "Distributed Architectures", 5.0));
    courses.add(course("IDX5060", "Game and Avatar Programming", 6.0));
    courses.add(course("IDX5711", "Intelligent Systems", 5.0));
    courses.add(course("IDX5712", "Intelligent Systems - Project", 2.0));
    courses.add(course("IDY0303", "Agent-oriented Modelling and Multiagent Systems", 5.0));
    courses.add(course("ITX004P", "Practice", 5.0));
    courses.add(course("ITX829P", "Teaching Practice", 5.0));
    courses.add(course("ITX8530", "Software Development Team Project: procurement", 10.0));
    courses.add(course("ITX8540", "Research Based Software Development Project: Startup", 10.0));
    return courses;
  }

  private Faculty faculty(String name) {
    return new Faculty(nextId(), name);
  }

  private Course course(String code, String name, Double ects) {
    Course course = new Course();
    course.setId(nextId());
    course.setCode(code);
    course.setName(name);
    course.setEctsCredits(ects);
    course.setDescription("Description for " + name);
    return course;
  }
}
