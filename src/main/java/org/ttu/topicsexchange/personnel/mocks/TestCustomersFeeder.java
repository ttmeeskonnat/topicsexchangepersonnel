package org.ttu.topicsexchange.personnel.mocks;

import eu.codearte.jfairy.Fairy;
import org.springframework.stereotype.Component;
import org.ttu.topicsexchange.personnel.authentication.Role;
import org.ttu.topicsexchange.personnel.authentication.User;
import org.ttu.topicsexchange.personnel.model.customers.*;

import java.util.*;

import static java.util.stream.Collectors.toSet;
import static org.ttu.topicsexchange.personnel.mocks.TestDataFeeder.nextId;

@Component
public class TestCustomersFeeder {

  private static final int NUMBER_OF_COMPANIES = 20;
  public static final int NUMBER_OF_STUDENTS = 400;
  public static final int NUMBER_OF_TEACHERS = 10;

  private Fairy fairy = Fairy.create();
  private Random rand = new Random();

  private Teacher TEACHER_1 = teacher("John", "Doe");
  private Teacher TEACHER_2 = teacher("Chuck", "Norris");
  private Teacher TEACHER_3 = teacher("Pamela", "Anderson");

  private final Set<Customer> TEST_CUSTOMERS = prepareCustomers();
  private final Set<User> TEST_USERS = prepareUsers();

  public Set<Customer> getCustomers() {
    return TEST_CUSTOMERS;
  }

  public Set<User> getUsers() {
    return TEST_USERS;
  }

  public Set<Customer> getStudents() {
    return getCustomers()
      .stream()
      .filter(customer -> customer.getType().equals(Customer.Type.STUDENT))
      .collect(toSet());
  }

  public Set<Customer> getTeachers() {
    return getCustomers()
      .stream()
      .filter(customer -> customer.getType().equals(Customer.Type.TEACHER))
      .collect(toSet());
  }

  public Person getPerson(Long personId) {
    Optional<Customer> person = getCustomers().stream().filter(el -> el.getId().equals(personId)).findAny();
    return person.isPresent() ? (Person)person.get() : null;
  }

  private Set<Customer> prepareCustomers() {
    Set<Customer> customers = new HashSet<>();
    for (int i = 0; i < NUMBER_OF_STUDENTS; i++) {
      customers.add(student());
    }

    for (int i = 0; i < NUMBER_OF_COMPANIES; i++) {
      customers.add(company());
    }

    for (int i = 0; i < NUMBER_OF_TEACHERS; i++) {
      customers.add(teacher());
    }

    customers.add(TEACHER_1);
    customers.add(TEACHER_2);
    customers.add(TEACHER_3);

    return customers;
  }

  public Teacher getTeacher1() {
    return TEACHER_1;
  }

  public Teacher getTeacher2() {
    return TEACHER_2;
  }

  public Teacher getTeacher3() {
    return TEACHER_3;
  }

  public Student getRandomStudent() {
    return (Student)getShuffledCustomers()
      .stream()
      .filter(customer -> customer.getType().equals(Customer.Type.STUDENT))
      .findAny().get();
  }

  public Teacher getRandomTeacher() {
    return (Teacher)getShuffledCustomers()
      .stream()
      .filter(customer -> customer.getType().equals(Customer.Type.TEACHER))
      .findAny().get();
  }

  public Company getRandomCompany() {
    return (Company)getShuffledCustomers()
      .stream()
      .filter(customer -> customer.getType().equals(Customer.Type.COMPANY))
      .findAny().get();
  }

  private ArrayList<Customer> getShuffledCustomers() {
    ArrayList<Customer> customers = new ArrayList<>(getCustomers());
    Collections.shuffle(customers);
    return customers;
  }

  private Set<User> prepareUsers() {
    Set<User> users = new HashSet<>();
    users.add(user(TEACHER_1, "a", "b", Role.TEACHER));
    users.add(user(TEACHER_2, "chuck", "x", Role.TEACHER, Role.CURRICULUM_MANAGER));
    users.add(user(TEACHER_3, "pamela", "x", Role.TEACHER, Role.REVIEWER));
    return users;
  }

  private User user(Person person, String username, String password, Role... roles) {
    User user = new User();
    user.setRelatedPerson(person);
    user.setUsername(username);
    user.setPassword(password);
    user.addRole(roles);
    return user;
  }

  private Company company() {
    eu.codearte.jfairy.producer.company.Company fairyCompany = fairy.company();

    Company company = new Company();
    company.setId(nextId());
    company.setName(fairyCompany.name());
    company.setEmail(fairyCompany.email());
    return company;
  }

  private Teacher teacher() {
    Teacher teacher = new Teacher();
    fillPersonData(teacher);
    return teacher;
  }

  private Teacher teacher(String firstName, String lastName) {
    eu.codearte.jfairy.producer.person.Person fairyPerson = fairy.person();
    Teacher teacher = new Teacher();
    teacher.setId(nextId());
    teacher.setFirstName(firstName);
    teacher.setLastName(lastName);
    teacher.setPhone(fairyPerson.telephoneNumber());
    teacher.setEmail(fairyPerson.email());
    return teacher;
  }

  private Student student() {
    Student student = new Student();
    fillPersonData(student);
    student.setStudentCode(rand.nextInt(999999) + "iapm");
    return student;
  }

  private void fillPersonData(Person person) {
    eu.codearte.jfairy.producer.person.Person fairyPerson = fairy.person();
    person.setId(nextId());
    person.setFirstName(fairyPerson.firstName());
    person.setLastName(fairyPerson.lastName());
    person.setPhone(fairyPerson.telephoneNumber());
    person.setEmail(fairyPerson.email());
  }
}
