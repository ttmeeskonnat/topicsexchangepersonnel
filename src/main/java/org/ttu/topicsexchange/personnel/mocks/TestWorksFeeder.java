package org.ttu.topicsexchange.personnel.mocks;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ttu.topicsexchange.personnel.model.Mentorship;
import org.ttu.topicsexchange.personnel.model.Work;
import org.ttu.topicsexchange.personnel.model.WorkDescriptor;
import org.ttu.topicsexchange.personnel.model.customers.Customer;
import org.ttu.topicsexchange.personnel.model.customers.Student;
import org.ttu.topicsexchange.personnel.model.customers.Teacher;

import java.util.*;

import static org.ttu.topicsexchange.personnel.mocks.TestDataFeeder.nextId;
import static org.ttu.topicsexchange.personnel.model.Mentorship.Type.ASSIGNMENT;
import static org.ttu.topicsexchange.personnel.model.Mentorship.Type.INVITATION;
import static org.ttu.topicsexchange.personnel.model.WorkDescriptor.Type.PRACTICE;
import static org.ttu.topicsexchange.personnel.model.WorkDescriptor.Type.THESIS;

@Component
public class TestWorksFeeder implements InitializingBean {

  private static final int NUMBER_OF_WORKS = 30;

  @Autowired
  TestCustomersFeeder customersFeeder;

  private Set<Work> testWorks;
  private Set<Mentorship> testMentorships;

  @Override
  public void afterPropertiesSet() throws Exception {
    testWorks = prepareWorks();
    testMentorships = prepareMentorships();
  }

  public Collection<Work> getWorks() {
    return testWorks;
  }

  public Set<Mentorship> getMentorships() {
    return testMentorships;
  }

  public boolean hasMentorship(Mentorship mentorship) {
    return getMentorships()
      .stream()
      .filter(el -> el.getMentor().equals(mentorship.getMentor()))
      .filter(el -> el.getWork().equals(mentorship.getWork()))
      .findAny().isPresent();
  }

  public void addMentorship(Mentorship mentorship) {
    mentorship.setId(nextId());
    mentorship.getWork().addMentor(mentorship.getMentor());
    testMentorships.add(mentorship);
  }

  public void removeMentorship(Mentorship mentorship) {
    mentorship.getWork().removeMentor(mentorship.getMentor());
    testMentorships.remove(mentorship);
  }

  private Set<Work> prepareWorks() {
    Set<Work> works = new HashSet<>();
    Iterator<Customer> students = customersFeeder.getStudents().iterator();
    for (int i = 0; i < NUMBER_OF_WORKS ; i++) {
      boolean isThesis = i % 2 == 0;
      if (isThesis) {
        works.add(work("thesis_" + i, "description of thesis " + i, THESIS, customersFeeder.getRandomCompany(), (Student)students.next()));
      }
      else {
        works.add(work("practice_" + i, "description of practice " + i, PRACTICE, customersFeeder.getRandomCompany(), (Student)students.next()));
      }
    }
    return works;
  }

  private Set<Mentorship> prepareMentorships() {
    Set<Mentorship> mentorships = new HashSet<>();

    int i = 0;
    for (Work work : getWorks()) {
      Teacher teacher;
      if (i % 3 == 0) {
        teacher = customersFeeder.getTeacher1();
      }
      else if (i % 2 == 0) {
        teacher = customersFeeder.getTeacher2();
      }
      else {
        teacher = customersFeeder.getTeacher3();
      }

      mentorships.add(mentorship(work, teacher, i % 4 == 0 ? ASSIGNMENT : INVITATION));
      i++;
    }

    return mentorships;
  }

  private Mentorship mentorship(Work work, Teacher teacher, Mentorship.Type mType){
    Mentorship mentorship = new Mentorship();
    mentorship.setId(nextId());
    mentorship.setType(mType);
    mentorship.setWork(work);
    mentorship.setStudent(customersFeeder.getRandomStudent());
    mentorship.setMentor(teacher);
    return mentorship;
  }

  private Work work(String title, String description, WorkDescriptor.Type type, Customer author, Student assignee) {
    Work work = new Work();
    work.setId((nextId()));
    work.setTitle(title);
    work.setDescription(description);
    work.setType(type);
    work.setStatus(Work.Status.IN_PROGRESS);
    work.setAuthor(author);
    work.setCreationDate(new Date());
    work.setAssignee(assignee);
    return work;
  }
}
