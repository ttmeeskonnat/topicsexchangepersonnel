package org.ttu.topicsexchange.personnel.mocks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ttu.topicsexchange.personnel.authentication.User;
import org.ttu.topicsexchange.personnel.model.Proposal;
import org.ttu.topicsexchange.personnel.model.Work;
import org.ttu.topicsexchange.personnel.model.customers.Customer;
import org.ttu.topicsexchange.personnel.model.faculty.Faculty;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class TestDataFeeder {

  private static final AtomicLong ID_SEQ = new AtomicLong(1000);

  @Autowired
  TestCustomersFeeder customersFeeder;
  @Autowired
  TestWorksFeeder worksFeeder;
  @Autowired
  TestFacultyFeeder facultyFeeder;
  @Autowired
  TestProposalsFeeder proposalsFeeder;

  public Set<User> getUsers() {
    return customersFeeder.getUsers();
  }

  public Set<Proposal> getProposals() {
    return proposalsFeeder.getProposals();
  }

  public Set<Customer> getCustomers() {
    return customersFeeder.getCustomers();
  }

  public Collection<Work> getWorks() {
    return worksFeeder.getWorks();
  }

  public Set<Faculty> getFaculties() {
    return facultyFeeder.getFaculties();
  }

  public static long nextId() {
    return ID_SEQ.getAndIncrement();
  }
}