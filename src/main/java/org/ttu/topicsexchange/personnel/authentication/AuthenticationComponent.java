package org.ttu.topicsexchange.personnel.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ttu.topicsexchange.personnel.mocks.TestDataFeeder;

@Component
public class AuthenticationComponent {

  @Autowired
  TestDataFeeder testDataFeeder;

  // todo: this method should actually invoke business layer service
  public LoginResult authenticate(String username, String password) {
    for (User user : testDataFeeder.getUsers()) {
      if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
        return new LoginResult(user, true);
      }
    }

    return new LoginResult(false);
  }

  public void resetPassword(String username) {
    // invoke external service and wait for e-mail with new generated password
  }
}
