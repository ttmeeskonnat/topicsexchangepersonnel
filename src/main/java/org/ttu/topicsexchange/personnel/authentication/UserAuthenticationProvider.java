package org.ttu.topicsexchange.personnel.authentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Component("authenticationProvider")
public class UserAuthenticationProvider implements AuthenticationProvider {

  private static final Logger LOG = LoggerFactory.getLogger(UserAuthenticationProvider.class);

  @Autowired
  private AuthenticationComponent authenticationComponent;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String username = authentication.getName();
    String password = authentication.getCredentials().toString();

    LoginResult loginResult = authenticationComponent.authenticate(username, password);
    if (!loginResult.isAuthenticated()) {
      String error = String.format("Invalid credentials for user [%s]", username);
      throw new BadCredentialsException(error);
    }

    User user = loginResult.getUser();
    Collection<GrantedAuthority> authorities = getGrantedAuthorities(user.getRoles());
    return new UsernamePasswordAuthenticationToken(user, authentication, authorities);
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return authentication.equals(UsernamePasswordAuthenticationToken.class);
  }

  private Collection<GrantedAuthority> getGrantedAuthorities(Set<Role> roles) {
    return roles.stream().map(role -> new SimpleGrantedAuthority(role.name())).collect(Collectors.toList());
  }
}
