package org.ttu.topicsexchange.personnel.authentication;

import org.ttu.topicsexchange.personnel.model.customers.Person;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class User implements Serializable {

  private Person relatedPerson;

  private String username;
  private String password; // todo: this field is temporary here and only for test purposes
                           // do not forget to remove it after integration with business layer

  private Set<Role> roles = new HashSet<>();

  public Person getRelatedPerson() {
    return relatedPerson;
  }

  public void setRelatedPerson(Person relatedPerson) {
    this.relatedPerson = relatedPerson;
  }

  public String getId(){
    return relatedPerson.getId().toString();
  }

  public String getDisplayName() {
    return relatedPerson.getName();
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public void addRole(Role ... roles) {
    this.roles.addAll(Arrays.asList(roles));
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
