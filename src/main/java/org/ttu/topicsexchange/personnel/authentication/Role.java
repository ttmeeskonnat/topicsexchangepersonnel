package org.ttu.topicsexchange.personnel.authentication;

public enum Role {
  TEACHER, STUDENT, COMPANY, CURRICULUM_MANAGER, REVIEWER
}
