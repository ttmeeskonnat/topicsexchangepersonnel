package org.ttu.topicsexchange.personnel.authentication;

public class LoginResult {

  private User user;
  private boolean authenticated;

  public LoginResult(boolean authenticated) {
    this.authenticated = authenticated;
  }

  public LoginResult(User user, boolean authenticated) {
    this.user = user;
    this.authenticated = authenticated;
  }

  public boolean isAuthenticated() {
    return authenticated;
  }

  public User getUser() {
    return user;
  }
}
